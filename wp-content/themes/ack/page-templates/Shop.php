<?php
/**
 * Template Name: Shop Template
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();

?>
<main id="site-content" role="main">
<div class="productLandingPageWrapper">
<div class="main_cat_template_wrapper">
<div class="main_cat_template_container">
<div class="featured-image-block">
<?php $sliderimage = get_field("banner");
//echo '<pre>'; print_r($sliderimage);echo '</pre>';

?>
      <div class="featured-image-block p-relative">
          <div class="featuredImage">
             <img src="<?php echo $sliderimage['url'];?>">
          </div>
      </div>
  </div>
</div>
</div>
<div class="containerWrapperCenter">
<?php
$args = array(
    'taxonomy' => "product_cat",
    'hide_empty' => 1

);
$product_categories = get_terms($args);
//echo '<pre>'; print_r($product_categories );echo '</pre>';
echo '<div class="cateogryTabs productLandingPage"><div class="arrowsToNavigate leftOne"><span class="lnr lnr-chevron-left"></span></div><div class="scrollerMaker"><ul class="cateogryTabsParent">';
foreach ($product_categories as $product_category)
{
?>
<li><a href="<?php echo get_category_link($product_category->term_id); ?>"><?php echo $product_category->name; ?></a></li>

<?php
}
echo '</ul></div><div class="arrowsToNavigate rightOne"><span class="lnr lnr-chevron-right"></span></div></div>';
?>
<div class="productsSectionWrapper">
<div class="sectionSet sectionSetLatest">
<h2>Latest Release</h2>
<ul class="products">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 7,
    'product_cat' => 'latest-release',
);
$loop = new WP_Query($args);
if ($loop->have_posts())
{
    while ($loop->have_posts()):
        $loop->the_post();
?>

<li class="product type-product post-<?php echo $product->get_id(); ?> status-publish first instock product_cat-epic-mythology has-post-thumbnail shipping-taxable purchasable product-type-simple">
<a href="<?php echo esc_url(get_permalink($product->get_id())); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<?php echo $product->get_image(); ?>
<h2 class="woocommerce-loop-product__title"><?php echo esc_attr($product->get_title()); ?></h2>
<span class="price"><?php echo $product->get_price_html(); ?></span>
</a>
<!-- <a href="?add-to-cart=<?php echo $product->get_id(); ?>" rel="nofollow" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart added">ADD TO CART</a>
 --></li>

<?php
    endwhile;
}
wp_reset_postdata();
?>
<li class="viewallButton">
  <div class="imageToRelative">
    <img width="320" height="434" src="<?php echo home_url(); ?>/wp-content/uploads/2020/05/narasimha-reddy_cover.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
    <a href="<?php echo home_url('/index.php');?>/shopnew/" rel="nofollow" class="viewall">View all</a>
  </div>
  
</li>
</ul>
</div>
<div class="sectionSet sectionSetBestSelling">
<h2>Best Selling</h2>
<ul class="products">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 7,
    'product_cat' => 'best-selling',
);
$loop = new WP_Query($args);
if ($loop->have_posts())
{
    while ($loop->have_posts()):
        $loop->the_post();
?>

<li class="product type-product post-<?php echo $product->get_id(); ?> status-publish first instock product_cat-epic-mythology has-post-thumbnail shipping-taxable purchasable product-type-simple">
<a href="<?php echo esc_url(get_permalink($product->get_id())); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<?php echo $product->get_image(); ?>
<h2 class="woocommerce-loop-product__title"><?php echo esc_attr($product->get_title()); ?></h2>
<span class="price"><?php echo $product->get_price_html(); ?></span>
</a>
<!-- <a href="?add-to-cart=<?php echo $product->get_id(); ?>" rel="nofollow" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart added">ADD TO CART</a>
 --></li>

<?php
    endwhile;
}
wp_reset_postdata();
?>
<li class="viewallButton">
  <div class="imageToRelative">
    <img width="320" height="434" src="<?php echo home_url(); ?>/wp-content/uploads/2020/05/narasimha-reddy_cover.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
    <a href="<?php echo home_url('/index.php');?>/shopnew/" rel="nofollow" class="viewall">View all</a>
  </div>
  
</li>
</ul>
</div>
<div class="sectionSet sectionSetExclusive">
<h2>Exclusive offer</h2>
<ul class="products">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 7,
    'product_cat' => 'exclusive-offer',
);
$loop = new WP_Query($args);
$counttotal = $loop->post_count;

if ($loop->have_posts())
{
    while ($loop->have_posts()):
        $loop->the_post();
?>

<li class="product type-product post-<?php echo $product->get_id(); ?> status-publish first instock product_cat-epic-mythology has-post-thumbnail shipping-taxable purchasable product-type-simple">
<a href="<?php echo esc_url(get_permalink($product->get_id())); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<?php echo $product->get_image(); ?>
<h2 class="woocommerce-loop-product__title"><?php echo esc_attr($product->get_title()); ?></h2>
<span class="price"><?php echo $product->get_price_html(); ?></span>
</a>
<!-- <a href="?add-to-cart=<?php echo $product->get_id(); ?>" rel="nofollow" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart added">ADD TO CART</a>
 --></li>
<?php
    endwhile;
}
wp_reset_postdata();
?>
<li class="viewallButton">
  <div class="imageToRelative">
    <img width="320" height="434" src="<?php echo home_url(); ?>/wp-content/uploads/2020/05/narasimha-reddy_cover.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
    <a href="<?php echo home_url('/index.php');?>/shopnew/" rel="nofollow" class="viewall">View all</a>
  </div>
  
</li>
</ul>
</div>
</div>
</div>
</div>
</main>

<?php get_footer(); ?>
