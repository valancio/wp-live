<?php
/**
 * Template Name: In the news 
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">
				<div class="featured-image-block">
                    <div class="featured-image-block p-relative">
                        <div class="featuredImage">
							<?php $bannerImage =get_field('banner_image'); ?>
                           <img src="<?php echo $bannerImage['url']; ?>">
                        </div>
                    </div>
                </div>
    
	<div class="articleBlockContainer container-block">
	<h2><center><?php the_title(); ?></center></h2>
		<div class="related-posts-wrapper1 padding-xy-10">
   <div class="related-posts-container">
      <div class="related-posts display-grid" >
	   <?php 
	   $newsInformations =get_field('news_information');
	   foreach($newsInformations as $newsInformation){
	   ?>
         <div class="single-post related-post">
            <a class="link-to-post link" href="<?php  echo $newsInformation['url']; ?>">             
               <p class="title-post"><?php  echo $newsInformation['title']; ?></p>
               <p class="whats-in-post "><?php  echo $newsInformation['description']; ?></p>
            </a>
         </div>
	   <?php }  ?>
      </div>
     
   </div>
</div>
</div>
	  <div class="adds_footer"><?php echo get_field('bottom_ad',$ids); ?></div>
</main>

<?php get_footer(); ?>
