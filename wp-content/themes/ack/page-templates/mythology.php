<?php
/**
 * Template Name: Mythology Template
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>
<?php 
$taxonomies='mythology_category';
$postTypeName='mythologies';
$ids = get_the_ID();
$idsft = get_the_ID();
$args = array(
	'post_type' => 'mythologies',
	'posts_per_page'=>-1,
	'tax_query' => array(
		array(
			'taxonomy' => 'mythology_category',
			'field' => 'slug',
			'terms' => 'slider',
		),
	),
	);
$SliderDatas = new WP_query($args);

$chirtaKathaStories = get_field('amar_chirta_katha_stories');
$latestBooks = get_field('latest_books');
$buyBooksonline = get_field('buy_books_online');
$Newsletter = get_field('newsletter');
//echo '<pre>'; print_r($chirtaKathaStories); echo '</pre>';
?>
<div style="display:none;" class="posttypename"><?php echo $postTypeName;?></div>
<div style="display:none;" class="catslug"><?php echo $taxonomies;?></div>
 <main id="site-content" role="main">

        <div class="main_cat_template_wrapper">

            <div class="main_cat_template_container">

                <div class="slider_block block_1_top">

                    <div class="sliderWrapper mainWrapperSlider">
                        <?php if ($SliderDatas->have_posts()) : while ($SliderDatas->have_posts()) : $SliderDatas->the_post(); 
								$image = get_field('image');
								$listOfCategories = get_field('list_of_category');
								$background_color = get_field('background_color');
								$promotional_banner = get_field('promotional_banner_link');
								$need_to_color = get_field('need_to_color');
								//echo '<pre>'; print_r($need_to_color); echo '</pre>';
								//$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large'); ?>
						<a  target="<?php if($promotional_banner){ ?>_blank<?php } ?>" href="<?php if($promotional_banner){ echo $promotional_banner; }else {echo the_permalink(); }?>"><div class="single_slider single p-relative">
                            <img src="<?php echo  $image['url']; ?>"/>
                       <?php  if($need_to_color=='Yes') { ?>
							<div class="contentOverImage p-absolute width-50" style="background:<?php echo $background_color;  ?>">
							     <div class="contentContainer bg-color">
                                    <p class="title"><?php if(!empty($listOfCategories))  { echo $listOfCategories->name;} ?></p>
                                    <p class="mainTitle"><?php the_title();  // echo wp_trim_words( get_the_content(), 10); ?></b></p>
                                     <div class="articleReadTime">
                                    <span class="lnr lnr-history"></span><?php echo get_post_meta($post->ID, 'read_more_text',true); ?><span class="btn">Read</span>
                                    </div>
                                </div>
                            </div>
					   <?php } ?>
                        </div></a>
                        <?php  endwhile; endif;  ?>

                    </div>

                </div>
                <div class="category-breadcrumb">
                    <div class="categoryBreadcrumbContainer container-block">
                        <ul class='bradcrumbs-parent'>
                           <li class="with-links">
                               <a href="<?php echo bloginfo('url');?>" rel="nofollow">Home</a> <span class="divider-bread"> / </span>
                           </li>
                            <li class="no-link">
                                <?php echo $page_title = $wp_query->post->post_title; ?>
                            </li>
                        </ul>
                    </div>
                </div>
				 <div class="adds_footer"><?php echo get_field('top_ad',$ids); ?></div>
                <div class="articleBlockWrapper padding-xy-10">
                    <div class="articleBlockContainer container-block">

                        <div class="articleBlock title center-text">
                            <?php  echo get_field('sub_title',$ids);?>
                        </div>

                        <div class="articleBlock subtitle center-text">
						<?php echo get_field('content',$ids);?>                          
						  </div>

                        <div class="related-posts-wrapper padding-xy-10">
                            <div class="related-posts-container">
                               <div class="related-posts display-grid" id="lead-data-landing">
								<?php 
								$args1 = array(
									'post_type' => 'mythologies',
									'posts_per_page'=>-1,
									'tax_query' => array (
										array(
										'taxonomy' => 'mythology_category',
										'terms' => array('slider', 'latest-article'),
										'field' => 'slug',
										'operator' => 'NOT IN',
										)
									),
									);
								$postDatas1 = new WP_query($args1);
								$total = $postDatas1->post_count;
								$postDatas1 = new WP_query($args1);
								$total = $postDatas1->post_count;
								
								$useragent = $_SERVER['HTTP_USER_AGENT']; 
							$iPod = stripos($useragent, "iPod"); 
							$iPad = stripos($useragent, "iPad"); 
							$iPhone = stripos($useragent, "iPhone");
							$Android = stripos($useragent, "Android"); 
							$iOS = stripos($useragent, "iOS");
							//-- You can add billion devices 

$DEVICE = ($iPod||$iPad||$iPhone||$Android||$iOS||$webOS||$Blackberry||$IEMobile||$OperaMini);
							  if($DEVICE==true){	
								
								$args = array(
									'post_type' => 'mythologies',
									'posts_per_page'=>-1,
									'tax_query' => array (
										array(
										'taxonomy' => 'mythology_category',
										'terms' => array('slider', 'latest-article'),
										'field' => 'slug',
										'operator' => 'NOT IN',
										)
									),
									);
							  }
							  else
							  {
								  
								  $args = array(
									'post_type' => 'mythologies',
									'posts_per_page'=>8,
									 'post_status' => 'publish',
									'tax_query' => array (
										array(
										'taxonomy' => 'mythology_category',
										'field' => 'slug',
										'terms' =>  array('latest-article', 'slider' ),										
										'operator' => 'NOT IN',
										)
									),
									);
							  
							  }  
								$idss=array();
								$postDatas = new WP_query($args);
								$totalPost = $postDatas->max_num_pages;
								//echo '<pre>'; print_r($postDatas->max_num_pages); echo '</pre>';
								if ($postDatas->have_posts()) : while ($postDatas->have_posts()) : $postDatas->the_post(); 
								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
								$idss[] = $post->ID;
								?>
								 
                                   <div class="single-post related-post">
                                      <a class="link-to-post link" href="<?php the_permalink(); ?>">
                                          <img class="image_post" src="<?php echo $thumb[0]; ?>"/>
                                          <p class="title-post"><?php the_title(); ?></p>
                                          <p class="whats-in-post notmobile"><?php echo get_the_excerpt(); //the_excerpt();  // echo wp_trim_words( get_the_content(), 20); ?></p>
                                      </a>
                                   </div>
                                   <?php  endwhile; endif;  ?>
								   <input type="hidden" value="<?php echo $totalPost; ?>" class="totalPosts">
								  
								   <input type="hidden" value="<?php echo serialize($idss); ?>" class="idsval"> 
                               </div>
							     <input type="hidden" id="row" value="8">
								<input type="hidden" id="all" value="<?php echo $total; ?>">
							    <?php if (  $postDatas->max_num_pages > 1 ) { ?>
                                <div class="display-block button-wrapper load-more-wrapper load-more-landing">
									<a href="javascript:void(0);" class="link read-now button-design load-more load-more-landinga">Load More</a>
								</div>
									<?php  } ?>
                            </div>
                        </div>
                    </div>
                </div>
				<?php echo do_shortcode('[comic_of_the_month_mytho]'); ?>

				<?php if(!empty($chirtaKathaStories)) { ?>
                <div class="videoWrapper padding-xy-10 bg-color">
                    <div class="videoBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="video_content content_section">

                                    <p class="section_title"><?php echo $chirtaKathaStories['amar_chirta_katha_heading']; ?><span
                                                class="episode-no"><?php echo $chirtaKathaStories['episode_no']; ?></span></p>

                                    <p class="video-title title"><?php echo $chirtaKathaStories['amar_chirta_katha_title']; ?></p>

                                    <p class="subtitle italic-text">
                                        <?php echo $chirtaKathaStories['amar_chirta_katha_description']; ?>
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design">
                                            <?php echo $chirtaKathaStories['amar_chirta_katha_button_text']; ?>
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span> <?php echo $chirtaKathaStories['amar_chirta_katha_time_text']; ?>
                                    </div>

                                </div>
                            </div>

                            <div class="second-half">

                                <div class="image_of_book video-thumb">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         height="512px" id="Layer_1" style="enable-background:new 0 0 512 512;"
                                         version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve"><path
                                                fill="#fff"
                                                d="M405.2,232.9L126.8,67.2c-3.4-2-6.9-3.2-10.9-3.2c-10.9,0-19.8,9-19.8,20H96v344h0.1c0,11,8.9,20,19.8,20  c4.1,0,7.5-1.4,11.2-3.4l278.1-165.5c6.6-5.5,10.8-13.8,10.8-23.1C416,246.7,411.8,238.5,405.2,232.9z"
                                                style="&#10;"/></svg>
                                    <img src="<?php echo $chirtaKathaStories['video_image']['url']; ?>"/>
                                </div>
                                <div class="video-container">

                                    <div class="video-here">
                                        <div class="close-icon text-center"><p>X</p></div>
                                        <video  controls autoplay data-src="<?php echo $chirtaKathaStories['video']['url']; ?>" src=""></video>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                </div>
				<?php } ?>

                <div class="lastestBooksWrapper bg-color padding-xy-10">
                    <div class="lastestBooksContainer container-block">

                        <div class="lastestBooks title center-text">
                            <?php echo $latestBooks['latest_books_title']; ?>
                        </div>

                        <div class="lastestBooks subtitle center-text">
                            <?php echo $latestBooks['latest_books_description']; ?>
                        </div>

                        <div class="latest-books-wrapper padding-xy-10">
                            <div class="latest-books-container">
                                <div class="latest-books display-grid">
									<?php 
										$args = array(
											'post_type' => 'product',
											'posts_per_page'=>2,
											'orderby' => 'title',
											'order' => 'asc',
											'tax_query' => array(
											array(
												'taxonomy' => 'product_cat',
												'field' => 'slug',
												'terms' => 'latest-book-mythology',
											),
												),
											);
									    $latestArticles = new WP_query($args);
										if ($latestArticles->have_posts()) : while ($latestArticles->have_posts()) : $latestArticles->the_post(); 
							        	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
									   // echo $post->ID;
									    $shop_link_national = get_post_meta($post->ID,'shop_link_national',true);						
									     $shop_link_international = get_post_meta($post->ID,'shop_link_international',true);						
									    					
										$user_ip = getenv('REMOTE_ADDR');// the IP address to query
										$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
										//echo '<pre>'; print_r($query); echo '</pre>';
								    	?>
 
                                    <div class="single-book latest-book display-flex">
                                        <a class="link-to-book link flex-1-40" href="<?php echo the_permalink(); // if($query && $query['status'] == 'success' && $query['country']=='India') {  echo $shop_link_national; } else { echo $shop_link_international; } ?>">
                                            <img class="book-image"
                                                 src="<?php echo $thumb[0]; ?>"/>
                                                    </a>

                                        <div class="right-side-content flex-1-60 padding-left-20 padding-right-20">
                                            <a href="<?php  echo the_permalink(); //if($query && $query['status'] == 'success' && $query['country']=='India') {  echo $shop_link_national; } else { echo $shop_link_international; } ?>" class="link-to-book link title_book"><p class="book-title"><?php the_title(); ?></p></a>

                                            <p class="book-excerpt notmobile"><?php  echo wp_trim_words( get_the_content(), 22); //echo get_the_excerpt(); // ?></p>
                                            <a href="<?php echo the_permalink(); //if($query && $query['status'] == 'success' && $query['country']=='India') {  echo $shop_link_national; } else { echo $shop_link_international; } ?>" class="link-to-book link read-now notmobile">Buy now
                                                <span class="arrows">
                                                    <span class="lnr lnr-chevron-right"></span>
                                                    <span class="lnr lnr-chevron-right"></span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    
									<?php endwhile; endif;  ?>
									
                                </div>
                            </div>
                        </div>


                    </div>
                </div>



				
            </div>
        </div>
 <div class="backdrop-modal"></div>
  <div class="adds_footer"><?php echo get_field('bottom_ad',$idsft); ?></div>
    </main>

<?php get_footer(); ?>
