<?php

/*
* Template Name: Success-new
*
*/

get_header();
global $woocommerce;

    $notices = WC()->session->get('wc_notices', array());
    $customers = WC()->session->get('customer', array());
   //echo '<pre>'; print_r(WC()->session); echo '</pre>';
if(!empty($notices)) {
?>

    <main id="site-content" role="main">
	
	<?php foreach ( $notices as $notice ) :  ?>
	<div class="woocommerce-message"<?php //echo wc_get_notice_data_attr( $notice ); ?> role="alert">
	<?php $str = wc_kses_notice( $notice[0]['notice'] ); 
		preg_match_all('!\d+!', $str, $matches);
		echo $orderids=  $matches[0][0];
			
	?>
	
	</div>
<?php  endforeach; 


$test_order = new WC_Order($orderids);
$test_order_key = $test_order->order_key;

  $url = 'https://www.amarchitrakatha.com/checkout/thank-you/'.$orderids.'/?key='.$test_order_key;
wp_redirect($url);
?>

       </main>
<?php } else {  ?>
   <main id="site-content" role="main">
 <div class="woocommerce-order WooCommerceThankYouPage">
 	<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Thank you for shopping with us.
</p></div></main>
<?php } ?>

<?php get_footer(); ?>