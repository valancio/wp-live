<?php
/**
 * Template Name: Watch
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
$ids = get_the_ID();
$idsft = get_the_ID();
?>

<main id="site-content" role="main">

    <div class="main_cat_template_wrapper">
        <div class="category-breadcrumb">
            <div class="categoryBreadcrumbContainer container-block">
                <ul class="bradcrumbs-parent">
                    <li class="with-links">
                        <a href="<?php home_url(); ?>" rel="nofollow">Home</a>
                    </li>
                    <li class="no-link">
                        / <?php echo get_the_title($ID); ?>
                    </li>
                </ul>
            </div>
        </div>
		 <div class="adds_footer top"><?php echo get_field('top_ad',$ids); ?></div>

        <div class="adds_footer"></div>
        <div class="VideoBlockWrapper padding-xy-10">
            <div class="VideoBlockContainer container-block">

                <div class="title center-text">
                    <?php echo get_field('title'); ?>                 </div>

                <div class="subtitle center-text">
                    <?php echo get_field('description'); ?>
                </div>


            </div>
        </div>

        <div class="quiz videoContainer container-block">
            <div class="video">
               
               <div class="videoContainer withFrame">
                <div class="tvbutton firstTime" data-status="ON">
                    <div class="innerTVButton">                        
                    </div>
<div class="statusText">OFF</div>
                </div>
                <div class="tv-Updater">  <div id="topDiv"></div>
<div id="centerDiv"></div>
<div id="bottomDiv"></div>
    </div>
                 <video id="my-video" controls preload="auto" class="video-js" controls width="100%"></video>
                <?php
                $buttonsName = get_field('buttons_name');

                ?>   
               </div>

               <div class="align-left">
                    <button class="previous"><?php echo $buttonsName['previous']; ?></button>
                    <button class="next"><?php echo $buttonsName['next']; ?></button>
                    <button class="pull-right subsc" onclick="window.location.href='https://www.youtube.com/channel/UCaymvCOGWUB4kcOh0bXPZZg?sub_confirmation=1'" >Subscribe </button>
                    
                </div>        

            </div>
     
            <!--  <div class="video upcoming">
                <p class="text-light"> Next Upcoming Video</p>

                <div class="display-flex">

                    <div> <img src="https://localhost/ack/wp-content/uploads/2020/04/upnextthum.png" ></div>
                    <div class="content">
                        <h4>Lorem ipsum dolor sit amet EP 14</h4>
                        <p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet <br> Lorem ipsum dolor sit amet</p>
                        <span class="timing">3:30</span>
                    </div>
                </div>

             </div> -->


            <div class="clearfix"></div>
        </div>


    </div>
    </div>
    <div class="backdrop-modal"></div>
    <div class="adds_footer"></div>
	<div class="adds_footer bottom"><?php echo get_field('bottom_ad',$idsft); ?></div>
</main>


<script>
    // https://github.com/brightcove/videojs-playlist/blob/master/docs/api.md

    var videoList = [
        <?php  $video_links = get_field('video_link_&_images'); 	
		shuffle( $video_links );
        //echo '<pre>'; print_r($video_links); echo  '</pre>'; 
        foreach($video_links as   $video_link) {    ?>
        {
            sources: [{
                src: '<?php echo $video_link["video_url"];?>',
                type: '<?php echo $video_link["type"];?>',
                techOrder: ["youtube", "html5"]
            }],
            poster: '<?php echo $video_link["image"]["url"]; ?>'
        },
        <?php } ?>
    ];
    var player = videojs(document.querySelector('video'), {
        inactivityTimeout: 0
    });
    try {
        // try on ios
        player.volume(1);
        // player.play();
    } catch (e) {
    }
    //player.playlist(videoList, 4);/// play video 5
    player.playlist(videoList);
    document.querySelector('.previous').addEventListener('click', function () {
        player.playlist.previous();
    });
    document.querySelector('.next').addEventListener('click', function () {
        player.playlist.next();
    });
    document.querySelector('.jump').addEventListener('click', function () {
        player.playlist.currentItem(2); // play third
    });

    player.playlist.autoadvance(0); // play all

    Array.prototype.forEach.call(document.querySelectorAll('[name=autoadvance]'), function (el) {
        el.addEventListener('click', function () {
            var value = document.querySelector('[name=autoadvance]:checked').value;
            //alert(value);
            player.playlist.autoadvance(JSON.parse(value));
        });
    });

    /* ADD PREVIOUS */
    var Button = videojs.getComponent('Button');

    // Extend default
    var PrevButton = videojs.extend(Button, {
        //constructor: function(player, options) {
        constructor: function () {
            Button.apply(this, arguments);
            //this.addClass('vjs-chapters-button');
            this.addClass('icon-angle-left');
            this.controlText("Previous");
        },

        // constructor: function() {
        //   Button.apply(this, arguments);
        //   this.addClass('vjs-play-control vjs-control vjs-button vjs-paused');
        // },

        // createEl: function() {
        //   return Button.prototype.createEl('button', {
        //     //className: 'vjs-next-button vjs-control vjs-button',
        //     //innerHTML: 'Next >'
        //   });
        // },

        handleClick: function () {
            console.log('click');
            player.playlist.previous();
        }
    });

    /* ADD BUTTON */
    //var Button = videojs.getComponent('Button');

    // Extend default
    var NextButton = videojs.extend(Button, {
        //constructor: function(player, options) {
        constructor: function () {
            Button.apply(this, arguments);
            //this.addClass('vjs-chapters-button');
            this.addClass('icon-angle-right');
            this.controlText("Next");
        },

        handleClick: function () {
            console.log('click');
            player.playlist.next();
        }
    });

    // Register the new component
    videojs.registerComponent('NextButton', NextButton);
    videojs.registerComponent('PrevButton', PrevButton);
    //player.getChild('controlBar').addChild('SharingButton', {});
    player.getChild('controlBar').addChild('PrevButton', {}, 0);
    player.getChild('controlBar').addChild('NextButton', {}, 2);
    //player.controlBar.addChild('SharingButton', {}, 6);

    //var MyButton = player.controlBar.addChild(new MyButtonComponent(), {}, 6);

    //const ControlBar = videojs.getComponent('ControlBar');
    //ControlBar.prototype.options_.children.splice(ControlBar.prototype.options_.children.length - 1, 0, 'SharingButton');

    // Register the new component
    //videojs.registerComponent('SharingButton', SharingButton);
    //player.getChild('controlBar').addChild('SharingButton', {});
</script>
<?php get_footer(); ?>
