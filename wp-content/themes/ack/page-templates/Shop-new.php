<?php
/**
 * Template Name: Shop-landing Template
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();

?>
    <main id="site-content" role="main">
        <div class="main_cat_template_wrapper">
            <div class="main_cat_template_container">
                <div class="featured-image-block">
                    <?php $sliderimage = get_field("banner");
                    //$title = get_field("title");
                    ?>
                    <div class="featured-image-block p-relative">
                        <div class="featuredImage">
                            <img src="<?php echo $sliderimage['url']; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .active span.cat_name {
                color: #fff;
            }

            .best-seller h3 {
                text-align: center;
                margin-top: 0;
            }

            .best-seller div {
                border: 1px solid rgba(0, 0, 0, .3);
                padding: 20px;
                margin-bottom: 50px;
                border-radius: 5px;
                background: #f9f9f9;
            }

            .best-seller div li {
                background: #fff;
            }

            .rightProdutsListing .woocommerce div.products {
                display: grid;
                grid-template-columns: repeat(4, 1fr);
                column-gap: 3rem;
                row-gap: 3rem;
            }

            .rightProdutsListing .woocommerce div.products li.product img {
                min-height: 150px;
                max-height: 190px;
                object-fit: contain;
            }

            div.products li.product .woocommerce-loop-product__title {
                margin: 1.5rem 0 .5rem;
                font-size: 17px;
            }

            div li {
                list-style-type: none;
            }
        </style>
        <div class="product-wrappers-listing">
            <div class="left-filters"><?php echo do_shortcode('[wpf-filters id=1]'); ?></div>
            <div class="rightProdutsListing"><?php //echo do_shortcode('[products]'); ?>
                <div class="woocommerce columns-4 ">
                    <!--<div class="tab-block" id="tab-block">
		<ul class="tab-mnu">
			<?php

                    $args = array(
                        'taxonomy' => 'product_cat',
                        'hide_empty' => true,
                        'meta_query' => array(
                            array(
                                'key' => 'enable',
                                'value' => "Yes",
                                'compare' => 'LIKE'
                            )
                        )

                    );

                    $product_categories = get_terms($args);
                    foreach ($product_categories as $product_categorie) {
                        ?>
			    <li class="<?php if ($product_categorie->term_id == $_GET['filter_cat_list_1']) {
                            echo 'active';
                        } ?>" >
                 <a href="<?php home_url('/shopnew/'); ?>?filter_cat_list_1=<?php echo $product_categorie->term_id; ?>"><p><span class="lnr lnr-graduation-hat"></span><span class="cat_name"><?php echo $product_categorie->name; ?></span></p></a>
			   </li>
			     <?php } ?>

            </ul>
		</div>-->
                    <div class="best-seller"><h3>Deals</h3>
                        <div class="products columns-4">
                            <?php
                            $tax_queries = array(
                                array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'slug',
                                    'terms' => array('deals'),
                                ),
                            );
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'tax_query' => $tax_queries


                            );
                            $loop = new WP_Query($args);
                            if ($loop->have_posts()) {
                                while ($loop->have_posts()):
                                    $loop->the_post();
                                    global $product;
                                    ?>
                                    <li class="product">
                                        <a href="<?php echo esc_url(get_permalink($product->get_id())); ?>" class="">
                                            <?php echo $product->get_image(); ?>
                                            <h2 class="woocommerce-loop-product__title"><?php echo esc_attr($product->get_title()); ?></h2>
                                            <span class="price"><?php echo $product->get_price_html(); ?></span>
                                        </a>
                                        <a href="<?php echo esc_url(get_permalink($product->get_id())); ?>"
                                           rel="nofollow" data-product_id="" data-product_sku=""
                                           class="button product_type_simple add_to_cart_button ajax_add_to_cart added">Buy
                                            now</a>
                                    </li>
                                <?php
                                endwhile;
                            }
                            wp_reset_postdata();
                            ?>


                        </div>

                    </div>


                    <ul class="products columns-4">
                        <?php
                        $orderby = 'name';
                        $order = 'asc';
                        $hide_empty = true;
                        $cat_args = array(
                            //'orderby'    => $orderby,
                            //'order'      => $order,
                            'hide_empty' => $hide_empty,
                        );
                        //$product_categories = get_terms( 'product_cat', $cat_args);
                        $product_categories = get_terms(array(
                            'taxonomy' => 'product_cat',
                            'hide_empty' => true
                        ));
                        $term_cat_id = array();
                        foreach ($product_categories as $term_cat) {
                            $term_slug[] = $term_cat->slug;

                        }
                        $data = implode(',', $term_slug);
                        $user_ip = getenv('REMOTE_ADDR');// the IP address to query
                        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $user_ip));

                        $tax_queries = array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'slug',
                                //'terms'    =>$term_slug,
                                //'terms' => array('workshops', 'subscription', 'offers', 'epic-mythology', 'uncategorized'),

                                //'operator'      => 'IN'
                            ),
                        );

                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => -1,
                           // 'tax_query' => $tax_queries


                        );
                        $loop = new WP_Query($args);

                        $counttotal = $loop->post_count;

                        if ($loop->have_posts()) {
                            while ($loop->have_posts()):
                                $loop->the_post();
                                global $product;
                                $terms = get_the_terms($product->get_id(), 'product_cat');
                                $termsslug = $terms[0]->slug;

                                 $workshopyesno = get_post_meta($product->get_id(), 'workshop_product_display_for_international', true);
                                $workshop_class = get_post_meta($product->get_id(), 'workshop_class', true);
                                ?>
                                <li class="product type-product post-<?php echo $product->get_id(); ?> <?php if ($workshopyesno == 'yes') {
                                    echo $workshop_class;
                                } elseif ($workshopyesno == 'Both') {
                                    echo $workshop_class;
                                } else {
                                    echo $workshop_class;
                                } ?>  status-publish first instock product_cat-epic-mythology has-post-thumbnail shipping-taxable purchasable product-type-simple">
                                    <?php if ($termsslug == 'subscription' || $termsslug == 'workshops') { ?>
                                        <span class="badges"><?php echo $terms = $terms[0]->name; ?></span>
                                    <?php } ?>
                                    <a href="<?php echo esc_url(get_permalink($product->get_id())); ?>"
                                       class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                        <?php echo $product->get_image(); ?>
                                        <h2 class="woocommerce-loop-product__title"><?php echo esc_attr($product->get_title()); ?></h2>
                                        <?php if ($query && $query['status'] == 'success' && $query['country'] == 'India') { ?><?php } else { ?>
                                            <span class="price"><?php echo $product->get_price_html(); ?></span><?php } ?>
                                    </a>
                                    <?php if ($query && $query['status'] == 'success' && $query['country'] == 'India') {
                                        $amazon_link = get_post_meta($product->get_id(), 'amazon_link', true);
                                        $flip_kart_link = get_post_meta($product->get_id(), 'flip_kart_link', true);
                                        $amazon_button_name = get_post_meta($product->get_id(), 'amazon_button_name', true);
                                        $flip_kart_button_name = get_post_meta($product->get_id(), 'flip_kart_button_name', true);

                                        if ($termsslug != 'subscription' && $termsslug != 'workshops' && $workshopyesno != 'ALL') {
											
                                            if (!empty($amazon_link) && !empty($amazon_button_name)) {
                                                ?>
                                                <a href="<?php the_permalink(); ?>" rel="nofollow"
                                                   data-product_id="<?php //echo $product->get_id(); ?>"
                                                   data-product_sku=""
                                                   class="button product_type_simple add_to_cart_button ajax_add_to_cart added amazon_button"><?php echo "Buy now"; //echo $amazon_button_name; ?></a>

                                                <?php
                                            }
                                            if ($termsslug == 'offers') {
                                                ?>
                                                <a href="<?php the_permalink(); ?>" rel="nofollow"
                                                   data-product_id="<?php //echo $product->get_id(); ?>"
                                                   data-product_sku=""
                                                   class="button product_type_simple add_to_cart_button ajax_add_to_cart added">Buy
                                                    now</a>

                                                <?php

                                            }
									} elseif ($workshopyesno == 'ALL' || $termsslug != 'subscription' || $termsslug != 'workshops' || $termsslug != 'offers') {
												
                                            ?>
                                             <a href="<?php the_permalink(); ?>" rel="nofollow"
                                                   data-product_id="<?php //echo $product->get_id(); ?>"
                                                   data-product_sku=""
                                                   class="button product_type_simple add_to_cart_button ajax_add_to_cart added">Buy
                                                    now</a>
                                            <?php
                                        } else { ?>
                                            <span class="price"><?php echo $product->get_price_html(); ?></span>
                                            <a href="<?php the_permalink(); ?>" rel="nofollow"
                                               data-product_id="<?php //echo $product->get_id();
                                               ?>" data-product_sku=""
                                               class="button product_type_simple add_to_cart_button ajax_add_to_cart added">Buy
                                                now</a>

                                            <?php
                                        }


                                    } else {  ?>
                                        <a href="<?php the_permalink(); ?>" rel="nofollow"
                                           data-product_id="<?php //echo $product->get_id(); ?>" data-product_sku=""
                                           class="button product_type_simple add_to_cart_button ajax_add_to_cart added">Buy
                                            now</a>
                                    <?php } ?>
                                </li>
                            <?php
                            endwhile;
                        }
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>

            </div>
        </div>
    </main>

<?php get_footer(); ?>