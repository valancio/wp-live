<?php
/**
 * Template Name: quiz Template
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
global $wpdb;
$prefix = $wpdb->base_prefix;
$ids = get_the_ID();
$idsft = get_the_ID();
?>
<?php
/*echo 'According To score <br> <br>';
global $wpdb;
$prefix = $wpdb->base_prefix;
$quizScores = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_quizzes");
//echo '<pre>'; print_r($quizScores); echo '</pre>';
 foreach($quizScores as $quizScore)
 {
	 $nameCate= $quizScore->quiz_name;
	 $nameCate = explode(",", $nameCate);
	 if($nameCate[1]=='Grade5-6'){
		 echo 'Grade5-6';
		 echo "Name".$nameCate[0];
		 echo '<br>';
	 }
	 if($nameCate[1]=='ACK FANS') {
		  echo 'ACK FANS';
		  echo "Name".$nameCate[0];
		   echo '<br>';
	 }
	 if($nameCate[1]=='Grade7-8'){
		  echo 'Grade7-8';
		  echo "Name".$nameCate[0];
		   echo '<br>';
	 }
 }
/*
foreach($quizScores as $quizScore)
{
	echo 'Total Score -- '.$quizScore->correct_score.'<br>';
	$userID = $quizScore->user;
	$userInfo = get_userdata($userID);
	echo 'UserName -- '.$userName = $userInfo->user_login;
	?>
	 <img src="<?php echo esc_url( get_avatar_url( $userID ) ); ?>" />
	<?php
}


echo 'According To Month <br> <br>';

global $wpdb;
$prefix = $wpdb->base_prefix;
$quizDetails = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_results WHERE user !=1 AND MONTH(`time_taken_real`) = MONTH(CURRENT_DATE()) limit 15");
//echo '<pre>'; print_r($quizDetails); echo '</pre>';
foreach($quizDetails as $quizDetail)
{
	echo 'Total Score -- '.$quizDetail->correct_score.'<br>';
	$userID = $quizDetail->user;
	$userInfo = get_userdata($userID);
	echo 'UserName -- '.$userName = $userInfo->user_login;
	?>
	 <img src="<?php echo esc_url( get_avatar_url( $userID ) ); ?>" />
	<?php
}

*/

?>
<main id="site-content" role="main">
   <div class="main_cat_template_wrapper">
      <div class="main_cat_template_container">
        <div class="featured-image-block">
					<?php $sliderimage = get_field( "slider_image" );
							$title = get_field("title");

					?>
                    <div class="featured-image-block p-relative">
                        <div class="featuredImage">
                           <img src="<?php if(!empty($sliderimage)) { echo $sliderimage['url']; } ?>">
                        </div>

<!--                        <div class="post-title-single-post p-absolute">-->
<!--                            <p>--><?php //if(!empty($title)){ echo $title;}?><!--</p>-->
<!--                        </div>-->
                    </div>
                </div>
      </div>
      <div class="category-breadcrumb">
         <div class="categoryBreadcrumbContainer container-block QuizPageContainer">
            <ul class="bradcrumbs-parent">
               <li class="with-links">
                  <a href="<?php home_url();?>" rel="nofollow">Home</a>
               </li>
               <li class="no-link">
                  / <?php echo get_the_title( $ID ); ?>
               </li>
            </ul>
         </div>
      </div>
      <div class="adds_footer play"><?php echo get_field('top_ad',$ids); ?></div>
      <div class="articleBlockWrapper padding-xy-10">
         <div class="articleBlockContainer container-block">
            <div class="articleBlock title center-text">
               <?php $quiz_title = get_field("quiz_title");  if(!empty($quiz_title)) { echo $quiz_title ;}  ?>
            </div>
            <div class="articleBlock subtitle center-text">
              <?php $quiz_descriptions = get_field("quiz_descriptions");  if(!empty($quiz_descriptions)) { echo $quiz_descriptions ;}  ?>
            </div>
         </div>
      </div>
      <div class="quiz container-block">
         <div class="tab-block" id = "tab-block">
		 <?php
		 $Grade56s = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_quizzes where category='Grade5-6' AND deleted=0");
		 $Grade78s = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_quizzes where category='Grade7-8' AND deleted=0");
		 $ackfans = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_quizzes where category='ACK FANS' AND deleted=0"); 
		// echo "SELECT * FROM ".$prefix."mlw_quizzes where category='Grade5-6'";
		 $quizcategories = get_field("quiz_category_tabbing");
		 ?>
            <ul class="tab-mnu">
			<?php  $i=1; foreach($quizcategories as  $quizcategory) { ?>
               <li class="<?php if($i==1){ echo 'active';}?>" id="<?php echo $quizcategory['id']; ?>">
                  <p><?php echo $quizcategory['icons']; ?><span><?php echo $quizcategory['name']; ?></span></p>
			   </li>
			<?php $i++;} ?>

            </ul>
            <div class="tab-cont">
               <div class="tab-pane">
                  <ul class="quizlisting">
				  <?php   $j=1;
				         foreach($Grade56s as $quizScore)
						 {
							 $nameCate= $quizScore->quiz_name;
							 $nameCate = explode(",", $nameCate);


						  $slug =  sanitize_title($nameCate[0]);
					?>
                     <li>
                        <div class="box" >
                           <div class="content">
                              <p><strong>Quiz <?php echo $j; ?>:</strong> <?php echo $nameCate[0]; ?>  </p>
                           </div>
                           <div class="action">
                              <a href="<?php echo home_url('/index.php/quiz/'); ?><?php echo $slug ;?>"><button class="btn-playnow"> PLAY NOW </button></a>
                           </div>
                           <div class="clearfix"></div>

                        </div>
                     </li>
							 <?php  $j++;  } ?>
                  </ul>
               </div>
               <div class="tab-pane">
                  <ul class="quizlisting">
					<?php   $j=1;
				         foreach($Grade78s as $quizScore)
						 {
							 $nameCate= $quizScore->quiz_name;
							 $nameCate = explode(",", $nameCate);

							$slug =  sanitize_title($nameCate[0]);
					?>
                     <li>
                        <div class="box" >
                           <div class="content">
                              <p><strong><?php echo $j; ?>:</strong>  <?php echo $nameCate[0]; ?>   </p>
                           </div>
                           <div class="action">
                               <a href="<?php echo home_url('/index.php/quiz/'); ?><?php echo $slug ;?>"><button class="btn-playnow"> PLAY NOW </button></a>
                           </div>
                           <div class="clearfix"></div>

                        </div>
                     </li>
					<?php   $j++;} ?>
                  </ul>
               </div>
               <div class="tab-pane">
                  <ul class="quizlisting">
						<?php   $j=1;
				         foreach($ackfans as $quizScore)
						 {
							 $nameCate= $quizScore->quiz_name;
							 $nameCate = explode(",", $nameCate);

							$slug =  sanitize_title($nameCate[0]);
					?>
                     <li>
                        <div class="box" >
                           <div class="content">
                              <p><strong><?php echo $j; ?>:</strong> <?php echo $nameCate[0]; ?> </p>
                           </div>
                           <div class="action">
                              <a href="<?php echo home_url('/index.php/quiz/'); ?><?php echo $slug ;?>"> <button class="btn-playnow"> PLAY NOW </button></a>
                           </div>
                           <div class="clearfix"></div>

                        </div>
                     </li>
				<?php   $j++;} ?>
                  </ul>
               </div>
            </div>
         </div>
         <div class="leaderboard">
            <div class="top15">
               <div class="tab-block" id = "tab-block1">
                  <ul class="tab-mnu">
                     <li class = "active" >
						<?php $winnerImage =  get_field('winner_all_the_time');?>
                        <img src="<?php echo $winnerImage['url']; ?>" />
                        <?php echo get_field('winner_all_the_time_title');?>
                     </li>
                     <li>
						<?php $currentmonth =  get_field('winner_current_month');?>
                        <img src="<?php echo $currentmonth['url']; ?>" />
                        <?php echo get_field('winner_current_month_title');?>
                     </li>
                  </ul>
                  <div class="tab-cont" id="style-3">
                     <div class="tab-pane">
                        <ul class="player" >
						<?php
						
						

						$quizScores = $wpdb->get_results("SELECT SUM(point_score) as total,user FROM ".$prefix."mlw_results where user !=0 AND user !=2 group BY user ORDER BY total desc LIMIT 15");
						$i =1;
						foreach($quizScores as $quizScore) {
						  $userID = $quizScore->user;
						 $userInfo = get_userdata($userID);
						if(!empty($userInfo->user_login)){
							
						?>
                           <li>
                              <span class="pos">#<?php echo $i; ?></span>
                              <span class="info"> <img src="<?php echo esc_url( get_avatar_url( $userID ) ); ?>"><?php if(!empty($userInfo->user_login)) { echo $userInfo->user_login; }?></span>
                              <span class="points"><?php echo $quizScore->total; ?></span>
                           </li>
						<?php $i++;  } } ?>
                        </ul>
                     </div>
                     <div class="tab-pane">
                        <ul class="player" >
                          <?php
							$quizDetails = $wpdb->get_results("SELECT SUM(point_score) as total ,user FROM ack_mlw_results where user !=0 AND user !=2  AND MONTH(`time_taken_real`) = MONTH(CURRENT_DATE()) group BY user ORDER BY total desc limit 15");
								$i=1;
							foreach($quizDetails as $quizDetail) {
								 $userID = $quizDetail->user;
								$userInfo = get_userdata($userID);
								?>
							<li>
                              <span class="pos">#<?php echo $i;?></span>
                              <span class="info"> <img src="<?php echo esc_url( get_avatar_url( $userID ) ); ?>"><?php echo $userInfo->user_login; ?></span>
                              <span class="points"><?php echo $quizDetail->total; ?></span>
                           </li>
                           <?php $i++;} ?>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   </div>
   <div class="backdrop-modal"></div>
     <div class="adds_footer play"><?php echo get_field('bottom_ad',$idsft); ?></div>
</main>

<script>
// ghanshyam
jQuery(document).ready(function(){
	 jQuery('.forgrade78').click(function(){
    var before = jQuery('#tab-block .tab-mnu li.active');
    before.removeClass('active');
    $('#forgrade78').addClass('active');
	$('#forgrade78').trigger('click');
	$('.closed').css('display','none');
	$('.lnr-cross').css('display','none');
	$(".lnr-menu").removeAttr("style");
	$('html,body').animate({scrollTop : 400},
        'slow');
   });
   jQuery('.forgrade56').click(function(){
    var before = jQuery('#tab-block .tab-mnu li.active');
    before.removeClass('active');
    $('#forgrade56').addClass('active');
	$('#forgrade56').trigger('click');
	$('.closed').css('display','none');
	$(".lnr-menu").removeAttr("style");
	$('.lnr-cross').css('display','none');
	$('html,body').animate({scrollTop : 400},
        'slow');
   });
   
   jQuery('.forackfans').click(function(){
    var before = jQuery('#tab-block .tab-mnu li.active');
    before.removeClass('active');
    $('#forackfans').addClass('active');
	$('#forackfans').trigger('click');
	$('.closed').css('display','none');
		$(".lnr-menu").removeAttr("style");
	$('.lnr-cross').css('display','none');
	$('html,body').animate({scrollTop :400},
        'slow');
   });
});
//end ghanshyam

//dev 08/04/2020

// JavaScript Document

jQuery(document).ready(function(){

  var tabWrapper = jQuery('#tab-block'),
      tabMnu = tabWrapper.find('.tab-mnu  li'),
      tabContent = tabWrapper.find('.tab-cont > .tab-pane');

  tabContent.not(':first-child').hide();

  tabMnu.each(function(i){
    jQuery(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    jQuery(this).attr('data-tab','tab'+i);
  });

  tabMnu.click(function(){
    var tabData = jQuery(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show();
  });

  jQuery('#tab-block .tab-mnu > li').click(function(){
    var before = jQuery('#tab-block .tab-mnu li.active');
    before.removeClass('active');
    $(this).addClass('active');
   });

});



// tab second

jQuery(document).ready(function(){

  var tabWrapper = jQuery('#tab-block1'),
      tabMnu = tabWrapper.find('.tab-mnu  li'),
      tabContent = tabWrapper.find('.tab-cont > .tab-pane');

  tabContent.not(':first-child').hide();

  tabMnu.each(function(i){
    jQuery(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    jQuery(this).attr('data-tab','tab'+i);
  });

  tabMnu.click(function(){
    var tabData = $(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show();
  });

  jQuery('#tab-block1 .tab-mnu > li').click(function(){
    var before = $('#tab-block1 .tab-mnu li.active');
    before.removeClass('active');
    $(this).addClass('active');
   });

});
</script>

<?php get_footer(); ?>