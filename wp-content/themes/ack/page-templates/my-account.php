<?php
/**
 * Template Name: My account
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
if (!is_user_logged_in() ) {
wp_redirect(home_url('/index.php/login/'));
}
?>


<?php
$current_user = wp_get_current_user();

/*
 * @example Safe usage: $current_user = wp_get_current_user();
 * if ( ! ( $current_user instanceof WP_User ) ) {
 *     return;
 * }
 */
/*printf( __( 'Username: %s', 'textdomain' ), esc_html( $current_user->user_login ) ) . '<br />';
echo '<br>';
printf( __( 'User email: %s', 'textdomain' ), esc_html( $current_user->user_email ) ) . '<br />';
echo '<br>';
printf( __( 'User first name: %s', 'textdomain' ), esc_html( $current_user->user_firstname ) ) . '<br />';
echo '<br>';
printf( __( 'User last name: %s', 'textdomain' ), esc_html( $current_user->user_lastname ) ) . '<br />';
echo '<br>';
printf( __( 'User display name: %s', 'textdomain' ), esc_html( $current_user->display_name ) ) . '<br />';
echo '<br>';
printf( __( 'User ID: %s', 'textdomain' ), esc_html( $current_user->ID ) );*/

global $wpdb;
$prefix = $wpdb->base_prefix;
$quizScores = $wpdb->get_results("SELECT * FROM ".$prefix."mlw_results WHERE user =".$current_user->ID);
/*foreach($quizScores as $quizScore)
{
	echo 'quiz title -'. $quizScore->quiz_name;
	echo '<br>';
	echo 'date of played -'. $quizScore->time_taken;
	echo '<br>';
	echo 'total scores -'. $quizScore->correct_score;
	echo '<br>';
	echo 'point scores -'. $quizScore->point_score;
	echo '<br>';
}*/
$userID =  $current_user->ID;
$social_app_name = get_user_meta($userID,'social_app_name',true);
?>

<main id="site-content" role="main">

        <div class="main_cat_template_wrapper">


            <div class="categoryBreadcrumbContainer MyAccountContainer">

				<div class="profile">

                 <div class="P-details">
                   <?php if(!empty($userID)) { ?> <img class="P-image" src="<?php echo esc_url( get_avatar_url($userID) ); ?>" > <?php } ?>
                    <ul class="userDetails">
                        <?php if(!empty($current_user->user_login)) { ?>  <li> <div><span class="lnr lnr-user"></span><p class="userName afterIcon"><?php echo $current_user->user_login; ?></p></div></li><?php  }?>
                        <?php if(!empty($current_user->user_email)) { ?> <li> <div><span class="lnr lnr-envelope"></span>  <p class="userMail afterIcon"> <?php echo $current_user->user_email; ?></p></div></li><?php } ?>
                        <hr/>
                        <li> <p><span class="lnr lnr-power-switch"></span>  <a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></p>
						<?php if($social_app_name=='facebook') { ?>
 						<span class="text-light"> You are logged in with  <img src="<?php echo home_url('wp-content/uploads/2020/04/fb-sm.png'); ?>" ></span>
						<?php } elseif($social_app_name=='google') { ?>
                        <span class="text-light"> You are logged in with  <?php echo home_url('wp-content/uploads/2020/04/google.png/'); ?></span>
						<?php }  else { ?>
					  <span class="text-light"> You are logged in with user email and password</span>
						<?php } ?>
					 </li>
                    </ul>
                 </div>

                 <div class="P-info">
                    <div class="Quiz-played">
                        <p class="P-title"><span class="lnr lnr-dice"></span>  Quiz History </p>
                        <ul>
						<?php  if(!empty($quizScores)) {
						foreach($quizScores as $quizScore) {
						?>

                            <li>
                                <div class="Q-name"><p><?php echo $quizScore->quiz_name;?></p></div>
                                <div class="Q-status"><span class="time"><?php  echo  date('j M Y', strtotime($quizScore->time_taken));?></span> <span class="points"><?php echo  $quizScore->correct_score; ?></span></div>
                                <div class="clearfix"></div>
                            </li>
                        <?php }} else {
							echo "No Data Found";
						} ?>

                        </ul>

                    </div>
                    <div class="clearfix"></div>
                </div>


                 <div class="clearfix"></div>
                </div>


            </div>
            </div>
        </div>
 <div class="backdrop-modal"></div>
  <div class="adds_footer"></div>
    </main>
<?php get_footer(); ?>
