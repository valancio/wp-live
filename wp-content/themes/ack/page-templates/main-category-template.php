<?php

/*
* Template Name: Main Category Page
*
*/

get_header();
?>
    <main id="site-content" role="main">

        <div class="main_cat_template_wrapper">

            <div class="main_cat_template_container">

                <div class="slider_block block_1_top">

                    <div class="sliderWrapper mainWrapperSlider">
                        <div class="single_slider single p-relative">
                            <a class="link link-to-post" href="#">
                                <img src="/ack/wp-content/uploads/2020/03/slider-mythology.jpg"/>
                                <div class="contentOverImage p-absolute width-50">

                                    <div class="contentContainer bg-color">
                                        <p class="title">Epic Legends</p>
                                        <p class="mainTitle">The Story Of <b>Shivaji</b></p>
                                        <div class="articleReadTime">
                                            <span class="lnr lnr-history"></span> 5 Minute Read
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single_slider single p-relative">
                            <img src="/ack/wp-content/uploads/2020/03/slider-mythology.jpg"/>
                            <div class="contentOverImage p-absolute width-50">

                                <div class="contentContainer bg-color">
                                    <p class="title">Epic Legends</p>
                                    <p class="mainTitle">The Story Of <b>Shivaji</b></p>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span> 5 Minute Read
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>

                </div>
                <div class="category-breadcrumb">
                    <div class="categoryBreadcrumbContainer container-block">
<!--                        --><?php //be_taxonomy_breadcrumb('mythology_category'); ?>
                    </div>
                </div>
                <div class="articleBlockWrapper padding-xy-10">
                    <div class="articleBlockContainer container-block">

                        <div class="articleBlock title center-text">
                            Article
                        </div>

                        <div class="articleBlock subtitle center-text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        </div>

                        <div class="related-posts-wrapper padding-xy-10">
                            <div class="related-posts-container">
                                <div class="related-posts display-grid">

                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                    <div class="single-post related-post">
                                        <a class="link-to-post link" href="#">
                                            <img class="image_post"
                                                 src="/ack/wp-content/uploads/2020/03/articleImage.jpg"/>
                                            <p class="title-post">One Words Many Meanings</p>
                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="comicBlockWrapper padding-xy-10 bg-color">
                    <div class="comicBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="image_of_book">
                                    <img src="/ack/wp-content/uploads/2020/03/newbook.jpg"/>
                                </div>

                            </div>

                            <div class="second-half">

                                <div class="book_content">

                                    <p class="section_title">Comic of The Month</p>

                                    <p class="book_Title title">Tales of Hanuman</p>

                                    <p class="subtitle italic-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design" href="#">
                                            Read Now
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span> 5 Minute Read
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="videoWrapper padding-xy-10 bg-color">
                    <div class="videoBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="video_content content_section">

                                    <p class="section_title">Amar chitra Katha Stories <span
                                                class="episode-no">EP05</span></p>

                                    <p class="video-title title">The Story of Ganesha</p>

                                    <p class="subtitle italic-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design">
                                            Watch Now
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span> 5 Minute Read
                                    </div>

                                </div>
                            </div>

                            <div class="second-half">

                                <div class="image_of_book video-thumb">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         height="512px" id="Layer_1" style="enable-background:new 0 0 512 512;"
                                         version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve"><path
                                                fill="#fff"
                                                d="M405.2,232.9L126.8,67.2c-3.4-2-6.9-3.2-10.9-3.2c-10.9,0-19.8,9-19.8,20H96v344h0.1c0,11,8.9,20,19.8,20  c4.1,0,7.5-1.4,11.2-3.4l278.1-165.5c6.6-5.5,10.8-13.8,10.8-23.1C416,246.7,411.8,238.5,405.2,232.9z"
                                                style="&#10;"/></svg>
                                    <img src="/ack/wp-content/uploads/2020/03/video-image.jpg"/>
                                </div>
                                <div class="video-container">

                                    <div class="video-here">
                                        <div class="close-icon text-center"><p>X</p></div>
                                        <video autoplay
                                               data-src="/ack/wp-content/uploads/2020/03/Lord-Shiva-3D-Animation-32486.mp4"
                                               src=""></video>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                </div>


                <div class="lastestBooksWrapper bg-color padding-xy-10">
                    <div class="lastestBooksContainer container-block">

                        <div class="lastestBooks title center-text">
                            Latest Books
                        </div>

                        <div class="lastestBooks subtitle center-text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        </div>

                        <div class="latest-books-wrapper padding-xy-10">
                            <div class="latest-books-container">
                                <div class="latest-books display-grid">

                                    <div class="single-book latest-book display-flex">
                                        <a class="link-to-book link flex-1-40" href="#">
                                            <img class="book-image"
                                                 src="/ack/wp-content/uploads/2020/03/latest-books.jpg"/>
                                            <!--                                            <p class="title-post">One Words Many Meanings</p>-->
                                            <!--                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>-->
                                        </a>

                                        <div class="right-side-content flex-1-60 padding-left-20 padding-right-20">
                                            <a href="#" class="link-to-book link title_book"><p class="book-title">Lorem
                                                    Ipsum Dolor</p></a>

                                            <p class="book-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit, sed do eiusmod tempor incididuntLorem ipsum dolor sit amet,
                                                consectetur adipiscing elit, sed do eiusmod tempor incididuntLorem ipsum
                                                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                                incididunt...</p>
                                            <a href="#" class="link-to-book link read-now">Read now
                                                <span class="arrows">
                                                    <span class="lnr lnr-chevron-right"></span>
                                                    <span class="lnr lnr-chevron-right"></span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="single-book latest-book display-flex">
                                        <a class="link-to-book link flex-1-40" href="#">
                                            <img class="book-image"
                                                 src="/ack/wp-content/uploads/2020/03/latest-books.jpg"/>
                                            <!--                                            <p class="title-post">One Words Many Meanings</p>-->
                                            <!--                                            <p class="whats-in-post">Do you know all the meanings of vanara?</p>-->
                                        </a>

                                        <div class="right-side-content flex-1-60 padding-left-20 padding-right-20">
                                            <a href="#" class="link-to-book link title_book"><p class="book-title">Lorem
                                                    Ipsum Dolor</p></a>

                                            <p class="book-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit, sed do eiusmod tempor incididuntLorem ipsum dolor sit amet,
                                                consectetur adipiscing elit, sed do eiusmod tempor incididuntLorem ipsum
                                                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                                incididunt...</p>
                                            <a href="#" class="link-to-book link read-now">Read now

                                                <span class="arrows">
                                                    <span class="lnr lnr-chevron-right"></span>
                                                    <span class="lnr lnr-chevron-right"></span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="showCaseWrapper bg-image">
                    <div class="showCaseContainer container-block">
                        <div class="showCaseSection">
                            <div class="content-block">
                                <div class="showCase-title title center-text">
                                    Buy Books Online
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="showCaseContainer image-full-width display-block">
                        <img src="/ack/wp-content/uploads/2020/03/ShowCaseImage.jpg">
                    </div>
                </div>

                <div class="newsletterWrapper bg-image">
                    <div class="newsletterContainer container-block">


                        <div class="newsletterSection">
                            <div class="content-block">
                                <div class="newsletter-title title center-text">
                                    Subscribe To Our Newsletter
                                </div>
                                <div class="newsletter-subtitle subtitle center-text margin-bottom">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt
                                </div>
                                <div class="form-newsletter">
                                    <?php dynamic_sidebar('newsletter-section') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="backdrop-modal"></div>
    </main>
    <!-- #site-content -->

<?php /* get_template_part( 'template-parts/footer-menus-widgets' ); */ ?>

<?php get_footer(); ?>