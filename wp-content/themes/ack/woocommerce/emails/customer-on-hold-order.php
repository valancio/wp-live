<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
$address    = $order->get_formatted_billing_address();
$shipping   = $order->get_formatted_shipping_address();
$paymentinfo = $order->get_payment_method_title();
?>

 <title>Order Email template</title>
    <!-- font links -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;900&display=swap" rel="stylesheet">
    <style>
        a{text-decoration: none;}
        .grand-total{margin-bottom: 20px;}
        .grand-total thead{background: #eaeaea;}
        .grand-total thead th{padding: 8px 10px;}
        .grand-total tbody tr:nth-child(odd) { background: #f6f6f6;}
        .grand-total tbody td{border-bottom:1px dashed #eaeaea;}
        .grand-total td{padding: 5px 10px; font-size: 12px;}
        .grand-total tbody td:nth-child(3), .grand-total thead th:nth-child(3){text-align: center;}
        .grand-total thead th:last-child, .grand-total tbody td:last-child, .grand-total tfoot td{text-align: right;}
    </style>
  </head>

  <body style="margin:0px; padding:0px; font-family: 'Roboto', sans-serif;">
    <!-- email template -->
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" style="border:1px solid #e0e0e0; ">
      <tbody>
        <tr>
            <td style="">
                <table class="main_table" style="width:760px;padding: 15px 15px 0 15px; " bgcolor="#fff" width="760" cellspacing="0" cellpadding="0" align="center">
                    <!--Header section-->
                    <tbody>
                        <tr>
                            <td style="" class="" bgcolor="#ffe900" align="center">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding: 12px 0 12px 22px;">
                                    <tbody>
                                        <tr>
                                            <td><img src="http://dev.sunarctechnologies.com/splitorder/wp-content/uploads/2020/07/brandlogo.png" alt="" style="max-width: 77px;"></td>
                                            <td style="#000;">
                                               <p style="text-transform:uppercase; font-weight: 900; font-size:22px; text-align:end; padding-right: 60px; margin:0;"> India's Favourite Story Teller!</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table  style="width:760px; padding: 0 15px;" cellspacing="0" cellpadding="0" align="center" class="middle-content">
                    <tbody>
                        <tr>
                            <td style="font-size: 24PX; color: #000; font-weight: 900; padding: 36px 0 40px; text-transform: uppercase; text-align: center;">It's Ordered!</td>
                        </tr>
                        <tr>
						
                            <td style="font-size: 16px; color: #000; padding-bottom: 18px;"><?php echo sprintf( esc_html__( 'Hello %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ) . "\n\n"; ?></td>
                        </tr>
                        <tr>
                            <td  style="font-size: 14px; padding-bottom: 23px;">Thank you for your order!  It has been successfully placed and has been received at our end for further processing. <p>Your order has been forwarded to the fulfillment center and will be dispatched to you through our logistics partners.</p></td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; padding-bottom: 0px;">
                                If you have subscribed towards any of our Tinkle or National Geographic Magazine Subscriptions, please note that you will have access to the digital copies of these respective brands. Our subscription team will be sending you an email with the details on how you can access the respective Magazines through the apps.
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; padding-bottom: 23px;">
                                If you have any questions about your order please contact us at <a href="mailto:customerservice@ack-media.com"  style="color: #1e7ec8; text-decoration: underline;">customerservice@ack-media.com</a> or call us </br></br><a href="tel:022-49188881 / 2 " style="color: #000;">022-49188881 / 2</a> from Monday - Friday, 10 AM - 6 PM IST.
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; padding-bottom: 27px;">Please find your order details below:</td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; padding-bottom: 26px;">
                                <span style="font-size: 16px; ">Your <?php echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) ); ?>
                            </td> 
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="card" cellspacing="0" cellpadding="0" style="width:760px; margin-bottom: 18px; padding: 0 15px;" align="center">
                    <tr>
                        <td valign="top" style="border:1px solid #eaeaea; margin-right: 5px; width: 350px;">
						<p style="background-color: #fee502; font-size: 14px; font-weight: 900; padding: 7px 9px; margin:0 ;">Billing Information:</p>
                            <address class="address">
				<?php echo wp_kses_post( $address ? $address : esc_html__( 'N/A', 'woocommerce' ) ); ?>
				<?php if ( $order->get_billing_phone() ) : ?>
					<br/><?php echo wc_make_phone_clickable( $order->get_billing_phone() ); ?>
				<?php endif; ?>
				<?php if ( $order->get_billing_email() ) : ?>
					<br/><?php echo esc_html( $order->get_billing_email() ); ?>
				<?php endif; ?>
			</address>
                        </td>
                        <td width="10px"></td>
                        <td valign="top" style="border:1px solid #eaeaea; margin-left: 5px; width: 350px;">
                            <p style="background-color: #fee502; font-size: 14px; font-weight: 900; padding: 7px 9px; margin:0 ;">Payment Method:</p>
                            <p style="font-size: 14px; color: #000; font-weight: 900; padding: 25px 9px;"><?php echo $paymentinfo; ?></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="card" cellspacing="0" cellpadding="0" style="width:760px; margin-bottom: 18px; padding: 0 15px;" align="center">
                  					
					<tr>
                        <td valign="top" style="border:1px solid #eaeaea; margin-right: 5px; width: 350px;">
                           <p style="background-color: #fee502; font-size: 14px; font-weight: 900; padding: 7px 9px; margin:0 ;">Shipping Information:</p>

							<address class="address"><?php echo wp_kses_post( $shipping ); ?></address>
                        </td>
                        <td width="10px"></td>
                        <td valign="top" style="border:1px solid #eaeaea; margin-right: 5px; width: 350px;">
                            <p style="background-color: #fee502; font-size: 14px; font-weight: 900; padding: 7px 9px; margin:0 ;">Shipping Method:</p>
                            <p style="font-size: 14px; color: #000; padding: 10px 9px;">Free Shipping on this order!</p>
                        </td>
                    </tr>
                </table>
            </td>
         </tr>
        <tr>
            <td>
                <table style="width:730px; border:1px solid #eaeaea;" cellspacing="0" cellpadding="0" align="center" class="grand-total">
                    <thead style="font-size: 12px; font-weight: 900; text-align: left; ">
                        <th>Item</th>                      
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </thead>
                    <tbody>
			<?php
			echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				$order,
				array(
					'show_sku'      => true,
					'show_image'    => false,
					'image_size'    => array( 32, 32 ),
					'plain_text'    => $plain_text,
					'sent_to_admin' => $sent_to_admin,
				)
			);
			?>
		</tbody>
		<tfoot>
			<?php
			$item_totals = $order->get_order_item_totals();

			if ( $item_totals ) {
				$i = 0;
				foreach ( $item_totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
				}
			}
			if ( $order->get_customer_note() ) {
				?>
				<tr>
					<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
				<?php
			}
			?>
		</tfoot>
		
                </table>
            </td>
        </tr>

        <!-- footer -->
        <tr>
            <td>
                <table style="width:760px; padding:12px 0 30px;" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffe900">
                    <tbody>
                        <tr align="center">
                            <td style="font-size: 14px;"><span style="font-weight: 900;">Call </span><a href="tel:022-49188881" style="font-weight: 300; color: #000;">022-49188881 / 2 / 3 / 4 </a>| <span style="font-weight: 900;">Email: </span><a href="mailto:customerservice@ack-media.com" style="font-weight: 300; color: #1e7ec8; text-decoration: underline;">customerservice@ackmedia.com</a> | Mon to Fri - 10AM to 6PM IST</td>
                        </tr>
                        <tr align="center">
                            <td style="font-size: 14px; font-weight: 900; color: #000; padding-top: 20px;">GST No. 27AAACQ1195K1Z7</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
      </tbody>
      </table>