<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input(
			array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>
		<?php
		$terms = get_the_terms( $product->get_id(), 'product_cat' );
		
 $termsslug =$terms[0]->slug;
		//echo $product->get_id();
		$user_ip = getenv('REMOTE_ADDR');// the IP address to query
		$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
		 $workshopyesno = get_post_meta($product->get_id(),'workshop_product_display_for_international',true);
		if($query && $query['status'] == 'success' && $query['country']=='India' &&  $workshopyesno !='Both' && $workshopyesno !='ALL') { 
		
		$amazon_link = get_post_meta($product->get_id(),'amazon_link',true);
		$flip_kart_link = get_post_meta($product->get_id(),'flip_kart_link',true);
		$amazon_button_name = get_post_meta($product->get_id(),'amazon_button_name',true);
		$flip_kart_button_name = get_post_meta($product->get_id(),'flip_kart_button_name',true);
		if($termsslug !='subscription' && $termsslug !='workshops' ){
		if(!empty($amazon_link) && !empty($amazon_button_name)) {
		?>
		<a target="_blank" href="<?php echo get_post_meta($product->get_id(),'amazon_link',true);?>" name="add-to-cart" value="<?php // echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt custombtn Buy_on_marketplace"><?php echo $amazon_button_name; ?><?php //echo esc_html( $product->single_add_to_cart_text() ); ?></a>
		<?php }
if(!empty($flip_kart_link) && !empty($flip_kart_button_name)) { ?>
<a target="_blank" href="<?php echo $flip_kart_link;?>" rel="nofollow" data-product_id="<?php //echo $product->get_id(); ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart added flip_kart_button single_add_to_cart_button button alt custombtn singlepage_product"><?php echo $flip_kart_button_name; ?></a>

<?php } 
		}
		
		else { 
		?>
		
		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php
		}
			}
		
			elseif($termsslug=='offers' && $workshopyesno =='Both' ){
	
		?>
			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php
			}
			elseif($workshopyesno =='ALL' || $termsslug !='subscription' || $termsslug !='workshops' || $termsslug !='offers'){
	
		?>
			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php
			}

			else {  ?>
		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php }?>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
