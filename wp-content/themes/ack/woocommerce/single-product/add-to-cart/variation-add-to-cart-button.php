<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input(
		array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		)
	);

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
<?php

$terms = get_the_terms( $product->get_id(), 'product_cat' );
		
 $termsslug =$terms[0]->slug;
		//echo $product->get_id();
		$user_ip = getenv('REMOTE_ADDR');// the IP address to query
		$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
		 $workshopyesno = get_post_meta($product->get_id(),'workshop_product_display_for_international',true);
		if($query && $query['status'] == 'success' && $query['country']=='India' &&  $workshopyesno !='Both' && $workshopyesno !='ALL') { 
		$amazon_link = get_post_meta($product->get_id(),'amazon_link',true);
		$flip_kart_link = get_post_meta($product->get_id(),'flip_kart_link',true);
		$amazon_button_name = get_post_meta($product->get_id(),'amazon_button_name',true);
		$flip_kart_button_name = get_post_meta($product->get_id(),'flip_kart_button_name',true);
		if($termsslug !='subscription' && $termsslug !='workshops' ){
		if(!empty($amazon_link) && !empty($amazon_button_name)) {
		?>
		<a target="_blank" href="<?php echo get_post_meta($product->get_id(),'amazon_link',true);?>" name="add-to-cart" value="<?php // echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt custombtn"><?php echo $amazon_button_name; ?><?php //echo esc_html( $product->single_add_to_cart_text() ); ?></a>
		<?php }
if(!empty($flip_kart_link) && !empty($flip_kart_button_name)) { ?>
<a  target="_blank" href="<?php echo $flip_kart_link;?>" rel="nofollow" data-product_id="<?php //echo $product->get_id(); ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart added flip_kart_button single_add_to_cart_button button alt custombtn singlepage_product"><?php echo $flip_kart_button_name; ?></a>

<?php } 
		} else { 
		?>
		
		<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
	
		<?php
		}
			}   
			
		
			elseif($termsslug =='offers' && $workshopyesno =='Both' ){
				 ?>
			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php
			}
				elseif($workshopyesno =='ALL' || $termsslug !='subscription' || $termsslug !='workshops' || $termsslug !='offers'){
	
		?>
			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php
			}
			else { ?>
	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
			<?php }?>
	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
