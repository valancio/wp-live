<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$user_ip = getenv('REMOTE_ADDR');// the IP address to query
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
$terms = get_the_terms( $product->get_id(), 'product_cat' );
$termsslug =$terms[0]->slug;
 $workshopyesno = get_post_meta($product->get_id(),'workshop_product_display_for_international',true);
?>
<?php  if($query && $query['status'] == 'success' && $query['country']=='India' && $workshopyesno !='Both') { 
if($termsslug !='subscription' && $termsslug !='workshops' ){
?> <?php }  else {  ?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
<?php } } elseif($termsslug=='offers' && $workshopyesno =='Both') {  ?> 
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>


 <?php  } else { ?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
<?php }?>