
// end dev 08/04/2020 //
$(window).on('load',function(){
setTimeout(function(){
    $(".preloader").hide()

    if ($(window).width() > 1200) {
        $(".landing_PageSingle").each(function (index) {
            $(this).addClass("animated" + (index + 1));
        });
    }
},2000)
});

$(document).ready(function () {
    console.clear();
    console.log("\n" +
        "   db    8b    d8    db    88\"\"Yb      dP\"\"b8 88  88 88 888888 88\"\"Yb    db        88  dP 88  88    db    888888 88  88    db    \n" +
        "  dPYb   88b  d88   dPYb   88__dP     dP   `\" 88  88 88   88   88__dP   dPYb       88odP  88  88   dPYb     88   88  88   dPYb   \n" +
        " dP__Yb  88YbdP88  dP__Yb  88\"Yb      Yb      888888 88   88   88\"Yb   dP__Yb      88\"Yb  888888  dP__Yb    88   888888  dP__Yb  \n" +
        "dP\"\"\"\"Yb 88 YY 88 dP\"\"\"\"Yb 88  Yb      YboodP 88  88 88   88   88  Yb dP\"\"\"\"Yb     88  Yb 88  88 dP\"\"\"\"Yb   88   88  88 dP\"\"\"\"Yb \n");

    // For Homepage Animation



    // Slider in Main Categories

    $(".sliderWrapper.mainWrapperSlider").slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    //Video Popup and close button

    $(".video-container").hide();
    $(".video_content.content_section a.link.read-now.button-design , .video-thumb").click(function () {
        let videoSource = $(".video-container .video-here video").attr("data-src");
        console.log(videoSource);
        $(".backdrop-modal").addClass("active");
        $(".video-container").show();
        $("body").addClass("overflow-hidden");
        $(".video-container .video-here video").attr("src", videoSource);
        $(".video-container").addClass("active")
    });

    $(".close-icon.text-center").click(function () {
        $(".video-container").hide();
        $(".video-container .video-here video").attr("src", "");
        $(".backdrop-modal").removeClass("active");
        $("body").removeClass("overflow-hidden");
    });

    // Sticky Header

    let header = $("#site-header");
    let body = $("body");
    let sticky = header.offset().top;

    $(window).scroll(function () {
        if (window.pageYOffset > sticky) {
            header.addClass("sticky-header");
            body.addClass("sticky-active");

        } else {
            header.removeClass("sticky-header");
            body.removeClass("sticky-active");
        }
    });
    $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").addClass("active-submenus");
    $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").show();
    if ($(window).width() > 991) {


        $("div#menu-hamburger ul.menus-for-burger > li").hover(function () {
            $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").removeClass("active-submenus");
            $("div#menu-hamburger ul.menus-for-burger > li").removeClass("active-submenus");
            $(this).addClass("active-submenus");
            $("ul.menus-for-burger li > .sub-menu").hide();
            $(this).find(".sub-menu").show();
        }, function () {
            $("ul.menus-for-burger > li > .sub-menu").hide();
            $("ul.menus-for-burger > li.active-submenus > .sub-menu").show();
        })

    }
    $(".icons-hide").hide();
    $(".icons-show").show();
    $("div#menu-hamburger").hide();
    $(".toggle-inner.menu-toggle-icon").click(function (e) {
        e.preventDefault();
        console.log("fdsf");
        $(".icons-hide").toggle();
        $(".icons-show").toggle();
        $("div#menu-hamburger").toggle();
    });



    if ($(window).width()< 990) {
//    for Mobile Hamburger Menus
        $("div#menu-hamburger ul.menus-for-burger > li > .sub-menu ").slideUp();
        $(".caratIcon.MobileView").click(function (e) {
            e.preventDefault();
            $(this).parents("ul.menus-for-burger > li").find(" > ul.sub-menu").slideToggle();
            $(this).parents("ul.menus-for-burger > li").toggleClass("menu-is-open");

        })

    }
   $("div#menu-hamburger-main  ul.menus-for-burger > li>ul.sub-menu>li:nth-of-type(1)").addClass("active-submenus");
    $("div#menu-hamburger-main ul.menus-for-burger > li>ul.sub-menu>li:nth-of-type(1)>ul.sub-menu").show();
    
    $("div#menu-hamburger-main ul.menus-for-burger > li:nth-of-type(1)>ul.sub-menu>li").hover(function () {
		$("div#menu-hamburger-main ul.menus-for-burger > li>ul.sub-menu>li:nth-of-type(1)").removeClass("active-submenus");
		$("div#menu-hamburger-main ul.menus-for-burger > li>ul.sub-menu>li").removeClass("active-submenus");
		$(this).addClass("active-submenus");
        $("ul.menus-for-burger li > .sub-menu>li>.sub-menu").hide();
            $(this).find(".sub-menu").show();
});


$("<span class='fa fa-caret-right icon-mobile'></span>").insertAfter("ul.hamburger-menus-for-burger >li.menu-item-has-children>a");

/*Mobile menus*/
$("ul.hamburger-menus-for-burger >li.menu-item-has-children>span.icon-mobile,ul.hamburger-menus-for-burger >li.menu-item-has-children>a").click(function() {
	$(this).toggleClass("main-toggle-menu-mobile");
	$(this).siblings( "ul" ).slideToggle();
});

});



// Horizontal Scroll

$(document).ready(function(){
  const buttonRight = $(".arrowsToNavigate.rightOne");
  const buttonLeft = $(".arrowsToNavigate.leftOne");
    if($(".scrollerMaker").scrollLeft() <= 0){
        $(".cateogryTabs.productLandingPage").addClass("leftEmpty")    
    }

    buttonRight.click(function () {   
        $(".cateogryTabs.productLandingPage").removeClass("leftEmpty")
        $(".scrollerMaker").scrollLeft($(".scrollerMaker").scrollLeft() + ($(".scrollerMaker").width() - 20) )
    });
    buttonLeft.click(function () {
        $(".cateogryTabs.productLandingPage").removeClass("rightEmpty");
        $(".scrollerMaker").scrollLeft($(".scrollerMaker").scrollLeft() - ($(".scrollerMaker").width() - 20) )
        if($(".scrollerMaker").scrollLeft() <= 0){
            $(".cateogryTabs.productLandingPage").addClass("leftEmpty")    
        }
    });

    var widths=0;

    $(".productLandingPageWrapper .cateogryTabsParent li").each(function(){
    widths += $(this).outerWidth() + 30;
    })
    $(".productLandingPageWrapper .cateogryTabsParent").css("width", widths);


// BackToTop

    var btn = $('#backToTop');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });





// for the video look like tv


    $('div#topDiv').animate({
      //51% for chrome
      height: "51%"
      , opacity: 1
    }, 800);

    $('div#bottomDiv').animate({
          //51% for chrome
          height: "51%"
          , opacity: 1
        }, 800,
        function () {
          $('div#centerDiv').css({display: "block"}).animate({
            width: "0%",
            left: "50%"
          }, 800);
        }
    );
 
 $(".video div#my-video").fadeOut();  
$(".tvbutton").click(function () {

$(".tvbutton").removeClass("firstTime");
  if ($(this).attr("data-status") == "ON") {
    	$(this).attr("data-status","OFF")
           $(".video button.vjs-big-play-button").trigger("click")
    	$(".tvbutton .statusText").text("ON")
    $(".video div#my-video").fadeIn(300);
    $('div#topDiv').css({
      height: "",
      opacity:''
    });

    $('div#bottomDiv').css({          
      	height: "",
      	opacity: ''
    })       
	$('div#centerDiv').css({display: "none",
		width: " ",
		left: " "
	})
  }
   else if (
   	$(this).attr("data-status") == "OFF") {
   	$(this).attr("data-status","ON")
   $(".tvbutton .statusText").text("OFF")
   $(".video button.vjs-play-control.vjs-control.vjs-button.vjs-playing").trigger("click")

$(".video div#my-video").fadeOut(300);

    $('div#topDiv').animate({
      //51% for chrome
      height: "51%"
      , opacity: 1
    }, 800);

    $('div#bottomDiv').animate({
          //51% for chrome
          height: "51%"
          , opacity: 1
        }, 800,
        function () {
          $('div#centerDiv').css({display: "block"}).animate({
            width: "0%",
            left: "50%"
          }, 800);
        }
    );
  }
});



    })


// Slider for the posts in pages
$(window).on("load resize",function(e){
    if(window.innerWidth < 768) {
         $(".related-posts-wrapper .related-posts-container .related-posts").slick({
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: false,
            arrows:true
          }
        },
        {
          breakpoint: 599,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
                 dots: false,
            arrows:true
          }
        }
      ]
    });
    }else{
        if($('.related-posts-wrapper .related-posts-container .related-posts').hasClass('slick-initialized')){
            $('.related-posts-wrapper .related-posts-container .related-posts').slick('unslick');
        }
    }
});




