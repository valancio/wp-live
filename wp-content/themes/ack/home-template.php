<?php

/*
* Template Name: Home Page ACK
*
*/


get_header();
?>

    <main id="site-content" role="main">

        <div class="preloader">
            <img src="https://www.amarchitrakatha.com/wp-content/uploads/2020/08/ack_loader.gif">
        </div>
        <?php
        $index = 1;
        if (have_rows('home_page_blocks')):
            ?>
            <div class="landingPageContainer">
                <?php while (have_rows('home_page_blocks')): the_row(); ?>

                    <div class='landing_PageSingle block_<?php echo $index ?> rellax'>

                        <a class="linker" href="<?php the_sub_field('block_link'); ?>">
                            <div class="relativeContent">
                                <img class="mainBlockImage" src="<?php the_sub_field('image_block'); ?>">
                                <!--


                                -->
<!--                                --><?php //if ( $index == 1 ) {?>
<!--                                <div class="svgAnimations">-->
<!--                                    <div class="container">-->
<!--                                        <div class="svg-box">-->
<!--                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" x="0px"-->
<!--                                                 y="0px"-->
<!--                                                 viewBox="0 0 80 80" enable-background="new 0 0 80 80"-->
<!--                                                 xml:space="preserve">-->
<!--  <circle fill="#fff" cx="80" cy="80" r="80"/>-->
<!--  </svg>-->
<!--                                        </div>-->
<!--                                        <div class="circle delay1"></div>-->
<!--                                        <div class="circle delay2"></div>-->
<!--                                        <div class="circle delay3"></div>-->
<!--                                        <div class="circle delay4"></div>-->
<!--                                        <div class="circle delay5"></div>-->
<!--                                        <div class="circle delay6"></div>-->
<!--                                        <div class="circle delay7"></div>-->
<!--                                    </div>-->
<!---->
<!--                                </div>-->
<!---->
<!--                                <!---->
<!---->
<!--                                -->-->
<!---->
<!--                --><?php //}?>
                                <div class="absolute_content contentBlock">
                                    <h1 class="mainTitle LandingPage">
                                        <?php the_sub_field('title_block'); ?>
                                    </h1>
                                    <p class="subText LandingPage">
                                        <?php the_sub_field('sub_text'); ?>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <?php $index++ ?>
                    </div>

                <?php endwhile; ?>
            </div>

        <?php endif;

        ?>

    </main><!-- #site-content -->

<?php /* get_template_part( 'template-parts/footer-menus-widgets' ); */ ?>

<?php get_footer(); ?>