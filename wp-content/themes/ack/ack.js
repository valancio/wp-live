
// end dev 08/04/2020 //
$(window).on('load',function(){
setTimeout(function(){
    $(".preloader").hide()

    if ($(window).width() > 1200) {
        $(".landing_PageSingle").each(function (index) {
            $(this).addClass("animated" + (index + 1));
        });
    }
},2000)
});

$(document).ready(function () {
    console.clear();
    console.log("\n" +
        "   db    8b    d8    db    88\"\"Yb      dP\"\"b8 88  88 88 888888 88\"\"Yb    db        88  dP 88  88    db    888888 88  88    db    \n" +
        "  dPYb   88b  d88   dPYb   88__dP     dP   `\" 88  88 88   88   88__dP   dPYb       88odP  88  88   dPYb     88   88  88   dPYb   \n" +
        " dP__Yb  88YbdP88  dP__Yb  88\"Yb      Yb      888888 88   88   88\"Yb   dP__Yb      88\"Yb  888888  dP__Yb    88   888888  dP__Yb  \n" +
        "dP\"\"\"\"Yb 88 YY 88 dP\"\"\"\"Yb 88  Yb      YboodP 88  88 88   88   88  Yb dP\"\"\"\"Yb     88  Yb 88  88 dP\"\"\"\"Yb   88   88  88 dP\"\"\"\"Yb \n");

    // For Homepage Animation



    // Slider in Main Categories

    $(".sliderWrapper.mainWrapperSlider").slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    //Video Popup and close button

    $(".video-container").hide();
    $(".video_content.content_section a.link.read-now.button-design , .video-thumb").click(function () {
        let videoSource = $(".video-container .video-here video").attr("data-src");
        console.log(videoSource);
        $(".backdrop-modal").addClass("active");
        $(".video-container").show();
        $("body").addClass("overflow-hidden");
        $(".video-container .video-here video").attr("src", videoSource);
        $(".video-container").addClass("active")
    });

    $(".close-icon.text-center").click(function () {
        $(".video-container").hide();
        $(".video-container .video-here video").attr("src", "");
        $(".backdrop-modal").removeClass("active");
        $("body").removeClass("overflow-hidden");
    });

    // Sticky Header

    let header = $("#site-header");
    let body = $("body");
    let sticky = header.offset().top;

    $(window).scroll(function () {
        if (window.pageYOffset > sticky) {
            header.addClass("sticky-header");
            body.addClass("sticky-active");

        } else {
            header.removeClass("sticky-header");
            body.removeClass("sticky-active");
        }
    });
    $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").addClass("active-submenus");
    $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").show();
    if ($(window).width() > 991) {


        $("div#menu-hamburger ul.menus-for-burger > li").hover(function () {
            $("div#menu-hamburger ul.menus-for-burger > li:nth-of-type(1)").removeClass("active-submenus");
            $("div#menu-hamburger ul.menus-for-burger > li").removeClass("active-submenus");
            $(this).addClass("active-submenus");
            $("ul.menus-for-burger li > .sub-menu").hide();
            $(this).find(".sub-menu").show();
        }, function () {
            $("ul.menus-for-burger > li > .sub-menu").hide();
            $("ul.menus-for-burger > li.active-submenus > .sub-menu").show();
        })

    }
    $(".icons-hide").hide();
    $(".icons-show").show();
    $("div#menu-hamburger").hide();
    $(".toggle-inner.menu-toggle-icon").click(function (e) {
        e.preventDefault();
        console.log("fdsf");
        $(".icons-hide").toggle();
        $(".icons-show").toggle();
        $("div#menu-hamburger").toggle();
    });



    if ($(window).width()< 990) {
//    for Mobile Hamburger Menus
        $("div#menu-hamburger ul.menus-for-burger > li > .sub-menu ").slideUp();
        $(".caratIcon.MobileView").click(function (e) {
            e.preventDefault();
            $(this).parents("ul.menus-for-burger > li").find(" > ul.sub-menu").slideToggle();
            $(this).parents("ul.menus-for-burger > li").toggleClass("menu-is-open");

        })

    }

});


