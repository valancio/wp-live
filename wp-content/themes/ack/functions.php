<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'googleFonts-QuickSand', 'https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap' );
    wp_enqueue_style( 'googleFonts-Nunito', 'https://fonts.googleapis.com/css?family=Nunito&display=swap' );
    wp_enqueue_style( 'googleFonts-Niconne', 'https://fonts.googleapis.com/css?family=Niconne&display=swap' );
    wp_enqueue_style( 'googleFonts-Delius', 'https://fonts.googleapis.com/css?family=Delius|Handlee|Mali&display=swap' );
    wp_enqueue_style( 'linearFonts', 'https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js' );
    wp_enqueue_style( 'Icons-CSS', 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css' );
    wp_enqueue_script( 'jQuery-JS', 'https://code.jquery.com/jquery-3.4.1.min.js');
    wp_enqueue_script( 'Custom-Js', get_stylesheet_directory_uri(). '/js/ack.js');
    wp_enqueue_style( 'Slick-CSS', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
    wp_enqueue_script( 'Slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js');
    wp_enqueue_script( 'rellax', 'https://cdnjs.cloudflare.com/ajax/libs/rellax/1.0.0/rellax.min.js');
}

function custom_menus_locations() {
    register_nav_menus(
        array(
            'ack-media' => __( 'Footer Menu - ACK MEDIA' ),
            'customer-service' => __( 'Footer Menu - Customer Service' )
        )
    );
}
add_action( 'init', 'custom_menus_locations' );

 
function custom_menus_history() {
    register_nav_menus(
        array(
            'history' => __( 'History - History' ),
            'literature' => __( 'Literature - Literature' ),
            'mythologies' => __( 'Mythologies - Mythologies' ),
            'play' => __( 'Play - Play' ),

        )
    );
}
add_action( 'init', 'custom_menus_history' );
//Widget Register

function wpb_widgets_init() {

       register_sidebar( array(
        'name' =>__( 'Footers Contact Us', 'wpb'),
        'id' => 'sidebar-21',
        'description' => __( 'Appears on the static front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<p class="widget-title footer-menu-title">',
        'after_title' => '</p>',
    ) );

	register_sidebar( array(
        'name' =>__( 'Social Media', 'wpb'),
        'id' => 'sidebar-22',
        'description' => __( 'Appears on the static front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' =>__( 'Copyright Text', 'wpb'),
        'id' => 'sidebar-23',
        'description' => __( 'Appears on the static front page template', 'wpb' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s copy-right-block-widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => 'News Letter Section',
        'id' => 'newsletter-section',
        'description' =>'NewsLetterSection',
        'before_widget' => '<div id="%1$s" class="widget %2$s NewsLetterSection">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	
	  register_sidebar( array(
        'name' => 'Social Sharing',
        'id' => 'social-sharing',
        'description' =>'Social Sharing',
        'before_widget' => '<div id="%1$s" class="widget %2$s Social Sharing">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">', 
        'after_title' => '</h3>',
    ) );

    }

add_action( 'widgets_init', 'wpb_widgets_init' );



// Register Custom Post Type mythology
function create_mythology_cpt() {

	$labels = array(
		'name' => _x( 'mythology', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'mythology', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Mythology', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'mythology', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'mythology Archives', 'textdomain' ),
		'attributes' => __( 'mythology Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent mythology:', 'textdomain' ),
		'all_items' => __( 'All Mythology', 'textdomain' ),
		'add_new_item' => __( 'Add New mythology', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New mythology', 'textdomain' ),
		'edit_item' => __( 'Edit mythology', 'textdomain' ),
		'update_item' => __( 'Update mythology', 'textdomain' ),
		'view_item' => __( 'View mythology', 'textdomain' ),
		'view_items' => __( 'View mythology', 'textdomain' ),
		'search_items' => __( 'Search mythology', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into mythology', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this mythology', 'textdomain' ),
		'items_list' => __( 'mythology list', 'textdomain' ),
		'items_list_navigation' => __( 'mythology list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter mythology list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'mythology', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-post',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments', 'trackbacks', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => true,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'mythologies', $args );

}
add_action( 'init', 'create_mythology_cpt', 0 );
// Taxonomy mythology
add_action( 'init', 'create_mythology_custom_taxonomy', 0 );
function create_mythology_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'Mythology category', 'taxonomy general name' ),
    'singular_name' => _x( 'Mythology category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Mythology' ),
    'parent_item' => __( 'Parent Mythology' ),
    'parent_item_colon' => __( 'Parent Mythology:' ),
    'edit_item' => __( 'Edit Mythology' ),
    'update_item' => __( 'Update Mythology' ),
    'add_new_item' => __( 'Add New Mythology' ),
    'new_item_name' => __( 'New Mythology ' ),
    'menu_name' => __( 'Mythology Category' ),
  );

  register_taxonomy('mythology_category',array('mythologies'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'mythology-category' ),
  ));
}


// Register Custom Post Type history
function create_history_cpt() {

	$labels = array(
		'name' => _x( 'history', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'history', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'History', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'history', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'history Archives', 'textdomain' ),
		'attributes' => __( 'history Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent history:', 'textdomain' ),
		'all_items' => __( 'All History', 'textdomain' ),
		'add_new_item' => __( 'Add New history', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New history', 'textdomain' ),
		'edit_item' => __( 'Edit history', 'textdomain' ),
		'update_item' => __( 'Update history', 'textdomain' ),
		'view_item' => __( 'View history', 'textdomain' ),
		'view_items' => __( 'View history', 'textdomain' ),
		'search_items' => __( 'Search history', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into history', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this history', 'textdomain' ),
		'items_list' => __( 'history list', 'textdomain' ),
		'items_list_navigation' => __( 'history list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter history list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'history', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-post',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments', 'trackbacks', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => true,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'history_details', $args );

}
add_action( 'init', 'create_history_cpt', 0 );


// Taxonomy history
add_action( 'init', 'create_history_custom_taxonomy', 0 );
function create_history_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'History category', 'taxonomy general name' ),
    'singular_name' => _x( 'History category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All History' ),
    'parent_item' => __( 'Parent History' ),
    'parent_item_colon' => __( 'Parent History:' ),
    'edit_item' => __( 'Edit History' ),
    'update_item' => __( 'Update History' ),
    'add_new_item' => __( 'Add New History' ),
    'new_item_name' => __( 'New History ' ),
    'menu_name' => __( 'History Category' ),
  );

  register_taxonomy('history_category',array('history_details'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'history-category' ),
  ));
}



// Register Custom Post Type literature
function create_literature_cpt() {

	$labels = array(
		'name' => _x( 'literature', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'literature', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Literature', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'literature', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'literature Archives', 'textdomain' ),
		'attributes' => __( 'literature Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent literature:', 'textdomain' ),
		'all_items' => __( 'All Literature', 'textdomain' ),
		'add_new_item' => __( 'Add New literature', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New literature', 'textdomain' ),
		'edit_item' => __( 'Edit literature', 'textdomain' ),
		'update_item' => __( 'Update literature', 'textdomain' ),
		'view_item' => __( 'View literature', 'textdomain' ),
		'view_items' => __( 'View literature', 'textdomain' ),
		'search_items' => __( 'Search literature', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into literature', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this literature', 'textdomain' ),
		'items_list' => __( 'literature list', 'textdomain' ),
		'items_list_navigation' => __( 'literature list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter literature list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'literature', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-post',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments', 'trackbacks', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => true,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'literature_details', $args );

}
add_action( 'init', 'create_literature_cpt', 0 );



// Taxonomy literature
add_action( 'init', 'create_literature_custom_taxonomy', 0 );
function create_literature_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'Literature category', 'taxonomy general name' ),
    'singular_name' => _x( 'Literature category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Literature' ),
    'parent_item' => __( 'Parent Literature' ),
    'parent_item_colon' => __( 'Parent Literature:' ),
    'edit_item' => __( 'Edit Literature' ),
    'update_item' => __( 'Update Literature' ),
    'add_new_item' => __( 'Add New Literature' ),
    'new_item_name' => __( 'New Literature ' ),
    'menu_name' => __( 'Literature Category' ),
  );

  register_taxonomy('literature_category',array('literature_details'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'literature-category' ),
  ));
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {

	// loop
	foreach( $items as &$item ) {

		// vars
		$icon = get_field('menu_image', $item);

		//echo '<pre>'; print_r($icon); echo '</pre>';
		// append icon
		if( $icon ) {

			$item->title = ' <i class="menu-icon hamburger fa fa-'.$icon['title'].'"><img width="100px" height="100px" src='.$icon['url'].'></i>'. "<div class='menu-icons-text'>" . $item->title .'</div>'. "<div class='caratIcon MobileView $item->title'><i class='fas fa-caret-right'></i></div>";

		}

	}


	// return
	return $items;

}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

add_filter('nav_menu_css_class' , 'special_nav_class1' , 10 , 2);

function special_nav_class1 ($classes, $item) {

	//echo '<pre>'; print_r($classes);echo '</pre>';
    if (in_array('current-page-ancestor', $classes) || in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}



 function get_breadcrumb() {
        global $post;
		$mythology_category='mythology_category';
		$history_category='history_category';
		$literature_category='literature_category';
        echo "<ul class='bradcrumbs-parent'>";
        echo '<li class="with-links">
<a href="' . home_url() . '" rel="nofollow">Home</a></li><span class="divider-bread"> / </span>';
        if (is_single()) {
               $post_type_object = get_post_type_object($post->post_type);
                $archive_link = site_url() . '/' . $post_type_object->label;
			if(is_single() && 'history_details' == get_post_type()){
				 $categories = get_the_terms( $post->ID, $history_category );

			}
			if(is_single() && 'mythologies' == get_post_type()){
			   $categories = get_the_terms( $post->ID, $mythology_category );
			}
			if(is_single() && 'literature_details' == get_post_type()){
			   $categories = get_the_terms( $post->ID, $literature_category );
			}
			   //echo'<pre>'; print_r($categories); echo '</pre>';
                echo '<li class="with-links"><a href="' . $archive_link . '" rel="nofollow">' . $post_type_object->label . '</a></li> <span class="divider-bread"> / </span>';
				$arraycounts = count($categories);
				if($arraycounts >2 ){
				$newarray = array_shift($categories);
				}

				if(!empty($newarray)){
				array_push($categories,$newarray);
				}
				//echo '<pre>'; print_r($categories); echo '</pre>';
				foreach($categories as $category) {
				$permalinks = get_term_link($category->term_id);
				echo '<li class="titles with-links"><a href="'.$permalinks.'">'.$category->name.'</a></li>';
				 echo ' <span class="divider-bread"> / </span>';
				}
				echo "<li class='no-link'><span>" . get_the_title() . "</span></li>";
            }
         elseif (is_category()) {
			 echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
            the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
        } elseif (is_page()) {
            if ($post->post_parent) {
                $checkParent = wp_get_post_parent_id($post->post_parent);
                if (!empty($checkParent) && $checkParent > 0) {
                    echo '<li class="with-links"><a href="' . get_permalink($checkParent) . '" rel="nofollow">' . get_the_title($checkParent) . '</a></li><span class="divider-bread"> / </span>';
                    echo '<li class="no-link"><a href="' . get_permalink($post->post_parent) . '" rel="nofollow">' . get_the_title($post->post_parent) . '</a></li><span class="divider-bread"> / </span>';
                } else {
                    echo '<li class="with-links"><a href="' . get_permalink($post->post_parent) . '" rel="nofollow">' . get_the_title($post->post_parent) . '</a></li>';
                }
            }

        } elseif (is_search()) {
            echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
            echo '"<em>';
            echo the_search_query();
            echo '</em>"';
        } elseif (is_404()) {
            echo "<li><span>404</span></li>";
        }
        echo "</ul>";
    }

	function be_taxonomy_breadcrumb($taxonomyname='',$CategoryID) {
		 $currentPageCatId = $CategoryID;
	    global $post;
	   $post_id = $post->ID;
	   echo "<ul class='bradcrumbs-parent'>";

	if($taxonomyname=='history_category'){
		$taxonomies='history_category';
		$postTypeName='history_details';
		//$Cat = wp_get_post_terms( $post_id, $taxonomies, array("fields" => "names"));
		//$termchildrens = get_terms($taxonomies,array('parent' => $currentPageCatId,'hide_empty' => 0,));
        $parents = $currentPageCatId;
		$category = get_the_category();
		$category_parent_id = $parents;
		if ( $category_parent_id != 0 ) {
			$category_parent = get_term( $category_parent_id,$taxonomies );
			$term_id = $category_parent->parent;
		    $subChild = $category_parent->name;
		} else {
			 $term_id = $category[0]->parent;
			 $subChild = $category[0]->name;
		}

		 $category_parent_ids = $term_id ;
		if ( $category_parent_ids != 0 ) {
			$category_parents = get_term( $category_parent_ids,$taxonomies );
			  $top_terms = $category_parents->parent;
			  $topParent = $category_parents->name;
		} else {
			 $topParent = $categorys[0]->name;
		}
		$category_parent_idss = $top_terms ;
		if ( $category_parent_idss != 0 ) {
			$category_parentss = get_term( $category_parent_idss,$taxonomies );
			    $mosttopParent = $category_parentss->name;
		} else {
			   $mosttopParent = $categorys[0]->name;
		}

		 $subChildterm_link = get_term_link($category_parent_id );
		 $topParentterm_link = get_term_link($category_parent_ids );
		 $mostParentterm_link = get_term_link($category_parent_idss);



		 echo '<li class="with-links"><a href="' . home_url() . '" rel="nofollow">Home</a></li>';
	  $Cat = wp_get_post_terms( $post_id, 'history_category', array("fields" => "names"));
        echo ' <span class="divider-bread"> / </span>';
	  echo '<li class="taxonomyname with-links"><a href="'. home_url('/history').'">History</a></li>';

	   if(!empty($mosttopParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$mostParentterm_link.'">'.$mosttopParent.'</a></li>';
		}
	  if(!empty($topParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$topParentterm_link.'">'.$topParent.'</a></li>';
		}
		if($subChild) {
		 echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$subChildterm_link.'">'. $subChild.'</a></li>';
		}
			if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }


	}
	elseif($taxonomyname=='mythology_category')
	{
		$taxonomies='mythology_category';
		$postTypeName='mythologies';
		//$Cat = wp_get_post_terms( $post_id, $taxonomies, array("fields" => "names"));
		//$termchildrens = get_terms($taxonomies,array('parent' => $currentPageCatId,'hide_empty' => 0,));
        $parents = $currentPageCatId;
		$category = get_the_category();
		$category_parent_id = $parents;
		if ( $category_parent_id != 0 ) {
			$category_parent = get_term( $category_parent_id,$taxonomies );
			$term_id = $category_parent->parent;
		    $subChild = $category_parent->name;
		} else {
			 $term_id = $category[0]->parent;
			 $subChild = $category[0]->name;
		}

		 $category_parent_ids = $term_id ;
		if ( $category_parent_ids != 0 ) {
			$category_parents = get_term( $category_parent_ids,$taxonomies );
			  $top_terms = $category_parents->parent;
			  $topParent = $category_parents->name;
		} else {
			 $topParent = $categorys[0]->name;
		}
		$category_parent_idss = $top_terms ;
		if ( $category_parent_idss != 0 ) {
			$category_parentss = get_term( $category_parent_idss,$taxonomies );
			    $mosttopParent = $category_parentss->name;
		} else {
			   $mosttopParent = $categorys[0]->name;
		}

		 $subChildterm_link = get_term_link($category_parent_id );
		 $topParentterm_link = get_term_link($category_parent_ids );
		 $mostParentterm_link = get_term_link($category_parent_idss);


	    echo '<li class="with-links"><a href="' . home_url() . '" rel="nofollow">Home</a></li>';
		 $Cat = wp_get_post_terms( $post_id, 'mythology_category', array("fields" => "names"));
		 echo ' <span class="divider-bread"> / </span>';
		 echo '<li class="taxonomyname with-links"><a href="'. home_url('/mytho').'">Mythology</a></li>';
		 if(!empty($mosttopParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$mostParentterm_link.'">'.$mosttopParent.'</a></li>';
		}

		if(!empty($topParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$topParentterm_link.'">'.$topParent.'</a></li>';
		}
		if($subChild) {
		 echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$subChildterm_link.'">'. $subChild.'</a></li>';
		}
		if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
	}
	elseif($taxonomyname=='literature_category')
	{
		$taxonomies='literature_category';
		$postTypeName='literature_details';
		//$Cat = wp_get_post_terms( $post_id, $taxonomies, array("fields" => "names"));
		//$termchildrens = get_terms($taxonomies,array('parent' => $currentPageCatId,'hide_empty' => 0,));
        $parents = $currentPageCatId;
		$category = get_the_category();
		$category_parent_id = $parents;
		if ( $category_parent_id != 0 ) {
			$category_parent = get_term( $category_parent_id,$taxonomies );
			$term_id = $category_parent->parent;
		    $subChild = $category_parent->name;
		} else {
			 $term_id = $category[0]->parent;
			 $subChild = $category[0]->name;
		}

		 $category_parent_ids = $term_id ;
		if ( $category_parent_ids != 0 ) {
			$category_parents = get_term( $category_parent_ids,$taxonomies );
			  $top_terms = $category_parents->parent;
			  $topParent = $category_parents->name;
		} else {
			 $topParent = $categorys[0]->name;
		}
		$category_parent_idss = $top_terms ;
		if ( $category_parent_idss != 0 ) {
			$category_parentss = get_term( $category_parent_idss,$taxonomies );
			    $mosttopParent = $category_parentss->name;
		} else {
			   $mosttopParent = $categorys[0]->name;
		}

		 $subChildterm_link = get_term_link($category_parent_id );
		 $topParentterm_link = get_term_link($category_parent_ids );
		 $mostParentterm_link = get_term_link($category_parent_idss);



		 echo '<li class="with-links"><a href="' . home_url() . '" rel="nofollow">Home</a></li>';
		 $Cat = wp_get_post_terms( $post_id, 'literature_category', array("fields" => "names"));
        echo ' <span class="divider-bread"> / </span>';
		echo '<li class="taxonomyname with-links"><a href="'. home_url('/literature').'">Literature</a></li>';
		 if(!empty($mosttopParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$mostParentterm_link.'">'.$mosttopParent.'</a></li>';
		}

		if(!empty($topParent)) {
	   echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$topParentterm_link.'">'.$topParent.'</a></li>';
		}
		if($subChild) {
		 echo ' <span class="divider-bread"> / </span>';
		echo '<li class="titles with-links"><a href="'.$subChildterm_link.'">'. $subChild.'</a></li>';
		}
        if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
	}
	 echo "</ul>";
}

add_action('save_post', 'assign_parent_terms', 10, 2);

function assign_parent_terms($post_id, $post){

    if($post->post_type  != 'mythologies')
        return $post_id;

    // get all assigned terms
    $terms = wp_get_post_terms($post_id, 'mythology_category' );
    foreach($terms as $term){
        while($term->parent != 0 && !has_term( $term->parent, 'mythology_category', $post )){
            // move upward until we get to 0 level terms
            wp_set_post_terms($post_id, array($term->parent), 'mythology_category', true);
            $term = get_term($term->parent, 'mythology_category');
        }
    }
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));



	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

}


  /**
     * Load post data using ajax taxonomy page
     */
    function load_team_by_ajax() {
		 $paged = $_POST['page'];
          $idsval = unserialize($_POST['idsval']);
          $paged = $_POST['page'];
        $divno = $_POST['divno'];
         $catid = $_POST['catid'];
         $catslug = $_POST['catslug'];
         $posttypename = $_POST['posttypename'];
        $args = array(
			'post_type' => $posttypename,
			'posts_per_page' => 4,
			 'post_status' => 'publish',
			'paged' => $paged,
			 'post__not_in' =>$idsval, 
			
		);

		$the_query = new WP_Query($args);

        $j = 1;
          if (!empty($the_query)) {
                        if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                          $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
                                            ?>
                                            <div class="single-post related-post latest-book-listing">
                                                <div class="image-book p-relative">
                                                    <img class="image_post"
                                                         src="<?php echo $thumb[0]; ?>"
                                                         srcset="<?php echo $thumb[0]; ?>" alt=""/>
                                                    <div class="hover-read-more p-absolute">
                                                        <a href="<?php the_permalink(); ?>"
                                                           class="link-to-book link read-now">
                                                    <span>Read now
                                                        <span class="arrows">
                                                                <span class="lnr lnr-chevron-right"></span>
                                                                <span class="lnr lnr-chevron-right"></span>
                                                        </span>
                                                    </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <a class="link-to-post link" href="<?php the_permalink(); ?>">
                                                    <p class="title-post"><?php the_title(); ?></p>
                                                </a>
                                                <p class="whats-in-post"><?php echo wp_trim_words(get_the_content(), 10); ?></p>
                                            </div>
                                        <?php endwhile;
                                        endif;
                                    } ?>
                                <?php
        wp_die();
    }

    add_action('wp_ajax_load_team_by_ajax', 'load_team_by_ajax');
    add_action('wp_ajax_nopriv_load_team_by_ajax', 'load_team_by_ajax');

/**
     * Load post data using ajax landing page
     */
    function load_team_by_ajax_landing() {
          $paged = $_POST['page'];
          $idsval = unserialize($_POST['idsval']);
		  //$idsval = asort($idsval);
		 //  print_r($idsval);
		 //array( 1883, 1181,1163,1139,1133,1154,1175,1123 );	
       // $divno = $_POST['divno'];
         //$catid = $_POST['catid'];
         $catslug = $_POST['catslug'];
         $posttypename = $_POST['posttypename'];
				$args = array(
					'post_type' =>$posttypename,
					'posts_per_page'=>4,
					'paged' => $paged,
					 'post_status' => 'publish',
					 'post__not_in' =>$idsval, 
					);
				$postDatas = new WP_query($args);
				//echo '<pre>'; print_r($postDatas); echo '</pre>';
				if ($postDatas->have_posts()) : while ($postDatas->have_posts()) : $postDatas->the_post(); 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
				?>
					   <div class="single-post related-post <?php echo $paged; ?>">
						  <a class="link-to-post link" href="<?php the_permalink(); ?>">
							  <img class="image_post" src="<?php echo $thumb[0]; ?>"/>
							  <p class="title-post"><?php the_title(); ?></p>
							  <p class="whats-in-post notmobile"><?php  echo get_the_excerpt(); //echo wp_trim_words( get_the_content(), 20); ?></p>
						  </a>
					   </div>
					   <?php  endwhile; endif; 
        wp_die();
    }

    add_action('wp_ajax_load_team_by_ajax_landing', 'load_team_by_ajax_landing');
    add_action('wp_ajax_nopriv_load_team_by_ajax_landing', 'load_team_by_ajax_landing');



function filter_plugin_updates( $value ) {
    unset( $value->response['quiz-master-next/mlw_quizmaster2.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' ); 


function filter_plugin_updates1( $value ) {
    unset( $value->response['woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php'] );
    return $value; 
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates1' ); 


// function that runs when shortcode is mythologies
function comic_of_the_month_mythologies() {
$mytho = get_page_by_path( 'mythology' );
$comicofMonth = get_field('comic_of_the_month',$mytho->ID);
if(!empty($comicofMonth)) {
?>
<div class="comicBlockWrapper padding-xy-10 bg-color">
                    <div class="comicBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="image_of_book">
                                    <img src="<?php echo $comicofMonth['image']['url']; ?>"/>
                                </div>

                            </div>

                            <div class="second-half">

                                <div class="book_content">

                                    <p class="section_title"><?php echo $comicofMonth['heading']; ?></p>

                                    <p class="book_Title title"><?php echo $comicofMonth['title']; ?></p>

                                    <p class="subtitle italic-text">
                                       <?php echo $comicofMonth['description']; ?>
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design" href="JavaScript:void(0)" >
												<?php echo do_shortcode('[dflip id="5540" type="button"]'.$comicofMonth['button_text'].'[/dflip]');?>
										  <?php // echo $comicofMonth['button_text'];  ?>
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span>  <?php echo $comicofMonth['time_with_text']; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				

<?php
}
}

add_shortcode('comic_of_the_month_mytho', 'comic_of_the_month_mythologies');





// function that runs when shortcode is literature
function comic_of_the_month_literature() {
$literature = get_page_by_path( 'literature' );
$comicofMonth = get_field('comic_of_the_month',$literature->ID);
if(!empty($comicofMonth)) {
?>
<div class="comicBlockWrapper padding-xy-10 bg-color">
                    <div class="comicBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="image_of_book">
                                    <img src="<?php echo $comicofMonth['image']['url']; ?>"/>
                                </div>

                            </div>

                            <div class="second-half">

                                <div class="book_content">

                                    <p class="section_title"><?php echo $comicofMonth['heading']; ?></p>

                                    <p class="book_Title title"><?php echo $comicofMonth['title']; ?></p>

                                    <p class="subtitle italic-text">
                                       <?php echo $comicofMonth['description']; ?>
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design" href="JavaScript:void(0)">
                                           <?php //echo $comicofMonth['button_text']; ?>
										   <?php echo do_shortcode('[dflip id="5540" type="button"]'.$comicofMonth['button_text'].'[/dflip]');?>
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span>  <?php echo $comicofMonth['time_with_text']; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<?php
}
}

add_shortcode('comic_of_the_month_lite', 'comic_of_the_month_literature');





// function that runs when shortcode is literature
function comic_of_the_month_history() {
$history = get_page_by_path('history');
$comicofMonth = get_field('comic_of_the_month',$history->ID);
if(!empty($comicofMonth)) {
?>
<div class="comicBlockWrapper padding-xy-10 bg-color">
                    <div class="comicBlockContainer container-block">


                        <div class="two-half-section display-flex">

                            <div class="first-half">

                                <div class="image_of_book">
                                    <img src="<?php echo $comicofMonth['image']['url']; ?>"/>
                                </div>

                            </div>

                            <div class="second-half">

                                <div class="book_content">

                                    <p class="section_title"><?php echo $comicofMonth['heading']; ?></p>

                                    <p class="book_Title title"><?php echo $comicofMonth['title']; ?></p>

                                    <p class="subtitle italic-text">
                                       <?php echo $comicofMonth['description']; ?>
                                    </p>

                                    <div class="display-block button-wrapper">
                                        <a class="link read-now button-design" href="JavaScript:void(0)">
                                           <?php // echo $comicofMonth['button_text']; ?>
										   <?php echo do_shortcode('[dflip id="5540" type="button"]'.$comicofMonth['button_text'].'[/dflip]');?>
                                        </a>
                                    </div>
                                    <div class="articleReadTime">
                                        <span class="lnr lnr-history"></span>  <?php echo $comicofMonth['time_with_text']; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<?php
}
}

add_shortcode('comic_of_the_month_his', 'comic_of_the_month_history');



add_shortcode ('woo_cart_but', 'woo_cart_but' );
/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
	ob_start();
 
        $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
        $cart_url = wc_get_cart_url();  // Set Cart URL
  
        ?>
        <div class="cartIcons"><a class="menu-item cart-contents" href="<?php echo $cart_url; ?>" title="My Basket">
	    <?php
        if ( $cart_count > 0 ) {
       ?>
            <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
        }
        ?>
        </a></div>
        <?php
	        
    return ob_get_clean();
 
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count( $fragments ) {
 
    ob_start();
    
    $cart_count = WC()->cart->cart_contents_count;
    $cart_url = wc_get_cart_url();
    
    ?>
    <a class="cart-contents menu-item" href="<?php echo $cart_url; ?>" title="<?php _e( 'View your shopping cart' ); ?>">
	<?php
    if ( $cart_count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php            
    }
        ?></a>
    <?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}

add_filter( 'wp_nav_menu_top-menu_items', 'woo_cart_but_icon', 10, 2 ); // Change menu to suit - example uses 'top-menu'

/**
 * Add WooCommerce Cart Menu Item Shortcode to particular menu
 */
function woo_cart_but_icon ( $items, $args ) {
       $items .=  '[woo_cart_but]'; // Adding the created Icon via the shortcode already created
       
       return $items;
} 

add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +
  
function woo_custom_single_add_to_cart_text() {
  
    return __( 'Buy now', 'woocommerce' );
  
}

add_filter( 'woocommerce_return_to_shop_redirect', 'bbloomer_change_return_shop_url' );
 
function bbloomer_change_return_shop_url() {
return home_url('/index.php/shop/');
}


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


add_filter( 'woocommerce_email_headers', 'sunarc_order_invoice_email_add_cc', 9999, 3 );
 
function sunarc_order_invoice_email_add_cc( $headers, $email_id, $order ) {
    if ( 'customer_invoice' == $email_id ) {
        $headers .= "Cc: <ackshop@ack-media.com>" . "\r\n"; // del if not needed
       
    }
    return $headers;
}



function bbloomer_add_quiz_score_endpoint() {
    add_rewrite_endpoint( 'quiz-score', EP_ROOT | EP_PAGES );
}
  
add_action( 'init', 'bbloomer_add_quiz_score_endpoint' );
  
  
// ------------------
// 2. Add new query var
  
function bbloomer_quiz_score_query_vars( $vars ) {
    $vars[] = 'quiz-score';
    return $vars;
}
  
add_filter( 'query_vars', 'bbloomer_quiz_score_query_vars', 0 );
  
  
// ------------------
// 3. Insert the new endpoint into the My Account menu
  
function bbloomer_add_quiz_score_link_my_account( $items ) {
    $items['quiz-score'] = 'Quiz Score';
    return $items;
}
  
add_filter( 'woocommerce_account_menu_items', 'bbloomer_add_quiz_score_link_my_account' );
  
  
// ------------------
// 4. Add content to the new endpoint
  
function bbloomer_quiz_score_content() {
?>
 <a href="https://www.amarchitrakatha.com/quiz-results/">Quiz Answers </a>
<?php 
}
  
add_action( 'woocommerce_account_quiz-score_endpoint', 'bbloomer_quiz_score_content' );  


add_filter( 'woocommerce_checkout_fields', 'bbloomer_shipping_phone_checkout' );
 
function bbloomer_shipping_phone_checkout( $fields ) {
   $fields['shipping']['shipping_phone'] = array(
      'label' => 'Phone',
      'required' => false,
      'class' => array( 'form-row-wide' ),
      'priority' => 95,
   );
   return $fields;
}
  
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'bbloomer_shipping_phone_checkout_display' );
 
function bbloomer_shipping_phone_checkout_display( $order ){
    echo '<p><b>Shipping Phone:</b> ' . get_post_meta( $order->get_id(), '_shipping_phone', true ) . '</p>';
}


function push_to_datalayer($order_id) {    
    $order = wc_get_order( $order_id );
	$user_ip = getenv('REMOTE_ADDR');// the IP address to query
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
//echo '<pre>'; print_r($query); echo '</pre>';
if($query && $query['status'] == 'success' && $query['country']=='India') {
    ?>
    <script type='text/javascript'>
	//This should work only on India
        window.dataLayer = window.dataLayer || [];
                 dataLayer.push({
                     'event' : 'transaction',
					 'currencyCode' : 'INR',
				      'ecommerce' : {
                         'purchase' : {
                             'actionField' : {
                                 'id': '<?php echo $order->get_order_number(); ?>',                                
                                 'revenue' : '<?php echo number_format($order->get_subtotal(), 2, ".", ""); ?>', 
                                 'tax': '<?php echo number_format($order->get_total_tax(), 2 ,".", ""); ?>',
                                 'shipping': '<?php echo number_format($order->calculate_shipping(), 2 , ".", ""); ?>'                                 
                             },
                                 'products': [
                                        <?php
                                         foreach ( $order->get_items() as $key => $item ) :
                                            $product = $order->get_product_from_item($item);
                                            $variant_name = ($item['variation_id']) ? wc_get_product($item['variation_id']) : '';
                                        ?>
                                    {
                                        'name' : '<?php echo $item['name']; ?>',
                                        'id' : '<?php echo $item['product_id']; ?>',
                                        'price' : '<?php echo number_format($order->get_line_subtotal($item), 2, ".", ""); ?>',                                       
                                        'category' : '<?php echo strip_tags($product->get_categories(', ', '', '')); ?>',                                        
                                        'quantity' : <?php echo $item['qty']; ?>
                                    },
                                <?php endforeach; ?>
                                 ]
                             }                            
                         }
                     });					 
        </script>                 
    <?php
} else { ?>
<script type='text/javascript'>
	//This should work only on USA/Other countries
        window.dataLayer = window.dataLayer || [];
                 dataLayer.push({
                     'event' : 'transaction',
					 'currencyCode' : 'USD',
				      'ecommerce' : {
                         'purchase' : {
                             'actionField' : {
                                 'id': '<?php echo $order->get_order_number(); ?>',                                
                                 'revenue' : '<?php echo number_format($order->get_subtotal(), 2, ".", ""); ?>', 
                                 'tax': '<?php echo number_format($order->get_total_tax(), 2 ,".", ""); ?>',
                                 'shipping': '<?php echo number_format($order->calculate_shipping(), 2 , ".", ""); ?>'                                 
                             },
                                 'products': [
                                        <?php
                                         foreach ( $order->get_items() as $key => $item ) :
                                            $product = $order->get_product_from_item($item);
                                            $variant_name = ($item['variation_id']) ? wc_get_product($item['variation_id']) : '';
                                        ?>
                                    {
                                        'name' : '<?php echo $item['name']; ?>',
                                        'id' : '<?php echo $item['product_id']; ?>',
                                        'price' : '<?php echo number_format($order->get_line_subtotal($item), 2, ".", ""); ?>',                                       
                                        'category' : '<?php echo strip_tags($product->get_categories(', ', '', '')); ?>',                                        
                                        'quantity' : <?php echo $item['qty']; ?>
                                    },
                                <?php endforeach; ?>
                                 ]
                             }                            
                         }
                     });					 
        </script> 
<?php 
}
}
add_action('woocommerce_thankyou' , 'push_to_datalayer');



function wpse_287199_woo_checkout_country( $fields ) {
    $geoData = WC_Geolocation::geolocate_ip();
    $countries = WC()->countries->get_countries();

    $fields['billing']['billing_country'] = array(
        'type' => 'select',
        'label'     => __('Country', 'woocommerce'),
        'options' => array(
            $geoData['country'] => $countries[$geoData['country']]
        ),
        'class' => array(
            'form-row-wide',
            'address-field',
            'update_totals_on_change'
        )
    );


    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'wpse_287199_woo_checkout_country' );