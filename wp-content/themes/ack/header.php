<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-174924058-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-174924058-1');
</script>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="google-site-verification" content="hMXTjVHSRjtynivxbW-zsB08iUCXehE_dbuMZ9jG47U" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
	<link rel="stylesheet" href='<?php echo get_stylesheet_directory_uri(); ?>/css/video-js.css'>
	<link rel="stylesheet" href='https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css'>
	<link rel="stylesheet" href='https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick-theme.css'>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/video.js'></script>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/videojs-ie8.min.js'></script>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/Youtube.js'></script>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/videojs-playlist.js'></script>


</head>

<body <?php body_class(); ?>>

<?php
wp_body_open();
?>

<?php

 if (is_page('home')) { ?>

    <header id="site-header" class="header-footer-group homepage_landing" role="banner">

        <div class="header-inner section-inner">

            <div class="header-titles-wrapper">

                <div class="header-titles">

                    <?php
                    // Site title or logo.
                    twentytwenty_site_logo();

                    // Site description.
                    twentytwenty_site_description();
                    ?>

                </div><!-- .header-titles -->

                <button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"
                        data-toggle-body-class="showing-menu-modal" aria-expanded="false"
                        data-set-focus=".close-nav-toggle">
						<span class="toggle-inner">
							<span class="toggle-icon">
								<?php twentytwenty_the_theme_svg('ellipsis'); ?>
							</span>
							<span class="toggle-text"><?php _e('Menu', 'twentytwenty'); ?></span>
						</span>
                </button><!-- .nav-toggle -->

            </div><!-- .header-titles-wrapper -->

            <div class="header-navigation-wrapper working-home">

                <?php
                if (has_nav_menu('primary') || !has_nav_menu('expanded')) {
                    ?>

                    <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e('Horizontal', 'twentytwenty'); ?>"
                         role="navigation">

                        <ul class="primary-menu reset-list-style">

                            <?php


                            if (has_nav_menu('primary')) {

                                wp_nav_menu(
                                    array(
                                        'container' => '',
                                        'items_wrap' => '%3$s',
                                        'theme_location' => 'primary',
                                    )
                                );

                            } elseif (!has_nav_menu('expanded')) {

                                wp_list_pages(
                                    array(
                                        'match_menu_classes' => true,
                                        'show_sub_menu_icons' => true,
                                        'title_li' => false,
                                        'walker' => new TwentyTwenty_Walker_Page(),
                                    )
                                );

                            }
                            ?>

                        </ul>

                    </nav><!-- .primary-menu-wrapper -->

                    <?php
                }

                if (true === $enable_header_search || has_nav_menu('expanded')) {
                    ?>

                    <div class="header-toggles hide-no-js">

                        <?php
                        if (has_nav_menu('expanded')) {
                            ?>

                            <div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">

                                <button class="toggle nav-toggle desktop-nav-toggle" data-toggle-target=".menu-modal"
                                        data-toggle-body-class="showing-menu-modal" aria-expanded="false"
                                        data-set-focus=".close-nav-toggle">
									<span class="toggle-inner menu-toggle-icon">
											<span class="toggle-icon">
                                            <span class="lnr lnr-menu icons-show"></span>
                                            <span class="lnr lnr-cross icons-hide"></span>
										</span>
									</span>
                                </button><!-- .nav-toggle -->

                            </div><!-- .nav-toggle-wrapper -->

                            <?php
                        }

                        if (true === $enable_header_search) {
                            ?>

                            <div class="toggle-wrapper search-toggle-wrapper">

                                <button class="toggle search-toggle desktop-search-toggle"
                                        data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal"
                                        data-set-focus=".search-modal .search-field" aria-expanded="false">
									<span class="toggle-inner">
										<?php twentytwenty_the_theme_svg('search'); ?>
										<span class="toggle-text"><?php _e('Search', 'twentytwenty'); ?></span>
									</span>
                                </button><!-- .search-toggle -->

                            </div>

                            <?php
                        }
                        ?>


                    </div><!-- .header-toggles -->
                    <?php
                }
                ?>

            </div><!-- .header-navigation-wrapper -->

        </div><!-- .header-inner -->

        <?php
        // Output the search modal (if it is activated in the customizer).
        if (true === $enable_header_search) {
            get_template_part('template-parts/modal-search');
        }
        ?>

    </header><!-- #site-header -->
<?php } else { ?>

    <header id="site-header" class="header-footer-group otherpage_Design" role="banner">

        <div class="header-inner section-inner header-controller">

            <div class="header-titles-wrapper">

                <?php

                // Check whether the header search is activated in the customizer.
                $enable_header_search = get_theme_mod('enable_header_search', true);

            

                    ?>

                    

                <div class="header-titles">

                    <?php
                    // Site title or logo.
                    twentytwenty_site_logo();

                    // Site description.
                    twentytwenty_site_description();
                    ?>

                </div>

            </div><!-- .header-titles-wrapper -->
            <div class="menu top-menu-header with-icons">


                
                             <!--                From Here to Here -->
                <div id="menu-hamburger-main" class="sidenav menu-hamburger-main-menu closed">
                    <ul class="menus-for-burger"> <?php
                        wp_nav_menu(array(
                            'menu' => 'Main menu',
                            'container' => '',
                            'items_wrap' => '%3$s',
                            'theme_location' => 'primary',
                            'container_id' => 'cssmenu'
                        ));
                        ?>
                    </ul>
                </div>
                <!--     .....           From Here to Here -->
        
            </div>
            <div class="header-navigation-wrapper working">

                <?php
                if (has_nav_menu('primary') || !has_nav_menu('expanded')) {
                    ?>

                    <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e('Horizontal', 'twentytwenty'); ?>"
                         role="navigation">

                        <ul class="primary-menu reset-list-style">

                            <?php
                            if (has_nav_menu('primary')) {

                                wp_nav_menu(
                                    array(
                                        'container' => '',
                                        'items_wrap' => '%3$s',
                                        'theme_location' => 'primary',
                                    )
                                );

                            } elseif (!has_nav_menu('expanded')) {

                                wp_list_pages(
                                    array(
                                        'match_menu_classes' => true,
                                        'show_sub_menu_icons' => true,
                                        'title_li' => false,
                                        'walker' => new TwentyTwenty_Walker_Page(),
                                    )
                                );

                            }
                            ?>

                        </ul>

                    </nav><!-- .primary-menu-wrapper -->
                    
                   
                    <?php
                } ?>

                <?php
                if (true === $enable_header_search || has_nav_menu('expanded')) {
                    ?>

                    <div class="header-toggles hide-no-js">
						<?php echo do_shortcode("[woo_cart_but]"); ?>
                        <?php
							// echo do_shortcode('[woocs]');

                        if (true === $enable_header_search) {
                            ?>

                            <div class="toggle-wrapper user-account-wrapper">

                                <a href="<?php echo site_url('/index.php/my-account/');?>" class="user-account"> 
                                   <span class="toggle-inner">
										<span class="lnr lnr-user"></span>
									</span>
                                </a>

                            </div>

                            <div class="toggle-wrapper search-toggle-wrapper">

                                <button class="toggle search-toggle desktop-search-toggle"
                                        data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal"
                                        data-set-focus=".search-modal .search-field" aria-expanded="false">
									<span class="toggle-inner">
										<?php twentytwenty_the_theme_svg('search'); ?>
									</span>
                                </button><!-- .search-toggle -->

                            </div>

                            <?php
                        }

                        if (has_nav_menu('expanded')) {
                            ?>

                            <div id="menu-hamburger" class="sidenav menu-hamburger closed">
                               <ul class="hamburger-menus-for-burger"> <?php
									  wp_nav_menu(array(
                                               'menu' => 'hamburger',
                                               'container' => '',
                                               'items_wrap' => '%3$s',
                                               'theme_location' => 'primary',
                                               'container_id' => 'cssmenu'
                                           ));
                                  
                                   ?>
                               </ul>
                            </div>

                            <div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">
                              
                                <button class="toggle nav-toggle desktop-nav-toggle">
                                     <span class="toggle-inner menu-toggle-icon sdfdsf">
                                             <span class="toggle-icon">

                                                 <span class="lnr lnr-menu icons-show"></span>
                                            <span class="lnr lnr-cross icons-hide"></span>
                                                                        </span>
                                                                    </span>
                                </button>

                                <!-- .nav-toggle -->

                            </div><!-- .nav-toggle-wrapper -->

                            <?php
                        }


                        ?>

                    </div><!-- .header-toggles -->
                    <?php
                }
                ?>
            </div><!-- .header-navigation-wrapper -->

        </div><!-- .header-inner -->

        <?php
        // Output the search modal (if it is activated in the customizer).
        if (true === $enable_header_search) {
            get_template_part('template-parts/modal-search');
        }
        ?>

    </header><!-- #site-header -->
<?php } ?>
<style>
    #cssmenu,
    #cssmenu ul,
    #cssmenu li,
    #cssmenu a {
        border: none;
        margin: 0;
        padding: 0;
        line-height: 1;
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        box-sizing: content-box;
    }

    #cssmenu {
        height: 37px;
        display: block;
        padding: 0;
        margin: 0;
        border: 1px solid;
        border-radius: 5px;
        width: auto;
        border-color: #080808;
    }

    #cssmenu,
    #cssmenu > ul > li > ul > li a:hover {
        background: #3c3c3c;
        background: -moz-linear-gradient(top, #3c3c3c 0%, #222222 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #3c3c3c), color-stop(100%, #222222));
        background: -webkit-linear-gradient(top, #3c3c3c 0%, #222222 100%);
        background: -o-linear-gradient(top, #3c3c3c 0%, #222222 100%);
        background: -ms-linear-gradient(top, #3c3c3c 0%, #222222 100%);
        background: linear-gradient(top, #3c3c3c 0%, #222222 100%);
    }

    #cssmenu > ul {
        list-style: inside none;
        padding: 0;
        margin: 0;
    }

    #cssmenu > ul > li {
        list-style: inside none;
        padding: 0;
        margin: 0;
        float: left;
        display: block;
        position: relative;
    }

    #cssmenu > ul > li > a {
        outline: none;
        display: block;
        position: relative;
        padding: 12px 20px;
        text-align: center;
        text-decoration: none;
        text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.4);
        font-weight: bold;
        font-size: 13px;
        font-family: Arial, Helvetica, sans-serif;
        border-right: 1px solid #080808;
        color: #ffffff;
    }

    #cssmenu > ul > li > a:hover {
        background: #080808;
        color: #ffffff;
    }

    #cssmenu > ul > li:first-child > a {
        border-radius: 5px 0 0 5px;
    }

    #cssmenu > ul > li > a:after {
        content: '';
        position: absolute;
        border-right: 1px solid;
        top: -1px;
        bottom: -1px;
        right: -2px;
        z-index: 99;
        border-color: #3c3c3c;
    }

    #cssmenu ul li.has-sub:hover > a:after {
        top: 0;
        bottom: 0;
    }

    #cssmenu > ul > li.has-sub > a:before {
        content: '';
        position: absolute;
        top: 18px;
        right: 6px;
        border: 5px solid transparent;
        border-top: 5px solid #ffffff;
    }

    #cssmenu > ul > li.has-sub:hover > a:before {
        top: 19px;
    }

    #cssmenu ul li.has-sub:hover > a {
        background: #3f3f3f;
        border-color: #3f3f3f;
        padding-bottom: 13px;
        padding-top: 13px;
        top: -1px;
        z-index: 999;
    }

    #cssmenu ul li.has-sub:hover > ul,
    #cssmenu ul li.has-sub:hover > div {
        display: block;
    }

    #cssmenu ul li.has-sub > a:hover {
        background: #3f3f3f;
        border-color: #3f3f3f;
    }

    #cssmenu ul li > ul,
    #cssmenu ul li > div {
        display: none;
        width: auto;
        position: absolute;
        top: 38px;
        padding: 10px 0;
        background: #3f3f3f;
        border-radius: 0 0 5px 5px;
        z-index: 999;
    }

    #cssmenu ul li > ul {
        width: 200px;
    }

    #cssmenu ul li > ul li {
        display: block;
        list-style: inside none;
        padding: 0;
        margin: 0;
        position: relative;
    }

    #cssmenu ul li > ul li a {
        outline: none;
        display: block;
        position: relative;
        margin: 0;
        padding: 8px 20px;
        font: 10pt Arial, Helvetica, sans-serif;
        color: #ffffff;
        text-decoration: none;
        text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.5);
    }

    #cssmenu ul ul a:hover {
        color: #ffffff;
    }

    #cssmenu > ul > li.has-sub > a:hover:before {
        border-top: 5px solid #ffffff;
    }
</style>


<?php

$user_ip = getenv('REMOTE_ADDR');// the IP address to query
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$user_ip));
//echo '<pre>'; print_r($query); echo '</pre>';
if($query && $query['status'] == 'success' && $query['country']=='India') { ?>
  <style> 
/*li#menu-item-611 {
    display: none;
}*/
.single .quantity {
    display: none !important;
}
  </style>
<?php } else { ?>
 <style>
/*li#menu-item-593  {
   display: none;
}*/
.show
{
	display:block !important;
}
.hide {
    display: none !important;
}
.page-template-Shop-new .products .hide {
    display: none !important;
}
  </style>
<?
<?php } 
// Output the menu modal.
get_template_part('template-parts/modal-menu');