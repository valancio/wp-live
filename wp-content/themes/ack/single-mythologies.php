<?php
/**
 * single-mythologies.php
  */
get_header();
$id = get_the_ID();
$images = get_field('single_page_image', $id);
$article_title = get_field('related_article_title',123);
$article_description = get_field('related_article_description',123);
//echo '<pre>'; print_r($images ); echo '</pre>';
$images = $images['url'];
//echo '<pre>'; print_r( $categories );echo '</pre>';
?>

<main id="site-content" class="mythologies-page-single" role="main">
    <div class="main_cat_single_template_wrapper">
        <div class="main_cat_single_template_container">
            <div class="articleContentBlockWrapper">
                <div class="featured-image-block">
                    <div class="featured-image-block p-relative">
                        <div class="featuredImage">
                            <img src="<?php echo $images; ?>">
                        </div>
<!---->
<!--                        <div class="post-title-single-post p-absolute">-->
<!--                            --><?php
//                            if (have_posts()) :
//                                while (have_posts()) : the_post();
//                                    the_title('<p class="title-over-image-single">', '</p>');
//                                endwhile;
//                            endif; ?>
<!--                        </div>-->
                    </div>
                </div>
                <div class="category-breadcrumb">
                    <div class="categoryBreadcrumbContainer container-block">
                        <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
                    </div>
                </div>

                <div class="containerForPosts wide-container-block">
                    <div class="articleContentContainer">

                        <?php
                        if (have_posts()) :
                            while (have_posts()) : the_post();
                                ?>
                                <div class="content-here after-image">
                                    <div class="image-here left-image">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="content-here right-content">

                                        <div class="post-title-single-post aboveContent title-area">
                                            <?php

                                            the_title('<p class="title-over-image-single">', '</p>');
                                            ?><p class="date-of-the-post">
                                                <span class="lnr lnr-calendar-full"></span> -  <?php $post_date = get_the_date( 'F j, Y' ); echo $post_date; ?> </p>
                                            <hr/>
                                        </div>

                                        <div class="showmoredefult"><p><?php the_content(); //$content = get_the_content(); echo $wordlimit = substr($content, 0, 800);?></p></div>
                                        <span class="showmore" style="display:none;"><?php the_content(); ?></span>

                                        <?php /* if(str_word_count($wordlimit) >= 40){ ?>
                                            <a href="JavaScript:void(0)" class="link-to-more-content link continue-reading">Continue Reading

                                                <span class="arrows">
                                                    <span class="lnr lnr-chevron-right"></span>
                                                    <span class="lnr lnr-chevron-right"></span>
                                                </span>
                                            </a>
                                        <?php  } */ ?>
                                       <!-- <a href="JavaScript:void(0)"  style="display:none;" class="link-to-more-content link continue-reading continue-reading-hide">Show less

                                            <span class="arrows">
                                                    <span class="lnr lnr-chevron-right"></span>
                                                    <span class="lnr lnr-chevron-right"></span>
                                                </span>
                                        </a> -->
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        else:
                            _e('Sorry, no pages matched your criteria.', 'textdomain');

                        endif; ?>
                    </div>

                    <!-- Other BLocks-->
					 <?php
                            $term_list = wp_get_post_terms( $post->ID, 'mythology_category', array( 'fields' => 'all' ) );
						
						   $term_slug =$term_list[0]->slug;
                            $argsdata = array(
                                'post_type' => 'mythologies',
                                'posts_per_page'=>3,
                                'post__not_in' => array( get_the_ID() ),
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'mythology_category',
                                        'field' => 'slug',
                                        'terms' => $term_slug,
                                    ),
                                ),
                            );
                            $articles = new WP_query($argsdata);
							$totalpost = $articles->post_count;
                            //echo '<pre>'; print_r( $articles );echo '</pre>';
                            ?>

                    <div class="articleBlockWrapper padding-xy-10 otherRelatedPosts">
                        <div class="articleBlockContainer container-block">
							 <?php if($totalpost >0) {?>
                            <div class="articleBlock title center-text">
                                <?php echo $article_title; ?>
                            </div>

                            <div class="articleBlock subtitle center-text">
                                <?php echo $article_description; ?>
                            </div>
							 <?php } ?>
                           

                            <div class="related-posts-wrapper padding-xy-10">
                                <div class="related-posts-container">
                                    <div class="related-posts display-grid">
                                        <?php if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post();
                                            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($articles->ID), 'large');


                                            ?>
                                            <div class="single-post related-post">
                                                <a class="link-to-post link" href="<?php the_permalink(); ?>">
                                                    <img class="image_post"
                                                         src="<?php   echo  $thumb[0];?>"/>
                                                    <p class="title-post"><?php  the_title();?></p>
                                                   <p class="whats-in-post notmobile"><?php  echo get_the_excerpt(); //echo wp_trim_words( get_the_content(), 20); ?></p>
					         </a>
                                            </div>
                                        <?php  endwhile; endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo do_shortcode('[comic_of_the_month_mytho]'); ?>
                <!--...Other BLocks-->
            </div>
        </div>
    </div>
</main>
<?php
// *** End Updated code ***** ///
get_footer(); ?>
