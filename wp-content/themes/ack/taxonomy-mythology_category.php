<?php
/**
 * taxonomy-mythology_category.php
 */
get_header();
$taxonomies='mythology_category';
$postTypeName='mythologies';
$tax = $wp_query->get_queried_object();
$CategoryID = $tax->term_id;


 $args = array(
    'post_type' => 'mythologies',
    'posts_per_page' => 8,
    'tax_query' => array(
        array(
            'taxonomy' => 'mythology_category',
            'field' => 'term_id',
            'terms' => $CategoryID
        )
    ),
);

$the_query = new WP_Query($args);
$totalPost = $the_query->max_num_pages;
$args1 = array(
    'post_type' => 'mythologies',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'mythology_category',
            'field' => 'term_id',
            'terms' => $CategoryID
        )
    ),
);

$the_query1 = new WP_Query($args1);
$totalpost = $the_query1->post_count;


$term = get_queried_object();

$sliderimage = get_field('slider_image',$term);
$slidertitle = get_field('slider_title',$term);
$title = get_field('title',$term);
$description = get_field('description',$term);
?>
<div style="display:none;" class="catname"><?php echo $CategoryID;?></div>
<div style="display:none;" class="posttypename"><?php echo $postTypeName;?></div>
<div style="display:none;" class="catslug"><?php echo $taxonomies;?></div>
    <main id="site-content" class="action-page-single" role="main">
        <div class="main_cat_single_template_wrapper">
            <div class="main_cat_single_template_container">
                <div class="articleContentBlockWrapper">
                    <div class="featured-image-block">
                        <div class="featured-image-block p-relative">
                            <div class="featuredImage">
                              <img src="<?php if(!empty($sliderimage)) { echo $sliderimage['url']; } ?>">
                            </div>
<!--                            <div class="post-title-single-post p-absolute">-->
<!--                              <p class="title-over-image-single">--><?php //if(!empty($slidertitle)) {  echo $slidertitle; }  ?><!--</p>-->
<!--								</div>-->
                        </div>
                    </div>
                    <div class="articleContentContainerAction container-block">

                        <div class="category-breadcrumb">
                            <div class="categoryBreadcrumbContainer container-block">
                                <?php be_taxonomy_breadcrumb('mythology_category',$CategoryID);
								//echo do_shortcode('[display_post_tax_terms_hierarchical taxonomy="mythology_category" separator="/"]');
								?>
                            </div>
                        </div>

                        <div class="result-Status-wrapper">
                            <div class="resultStatusContainer container-block">
							<?php   if($the_query->post_count > 0) { ?>
                                <p class="resultStatus-text loadResult">
                                    Showing 4 of <?php echo $total; ?> results
                                </p>
								<?php } ?>
                            </div>
                        </div>
						 <div class="adds_footer"><?php echo get_field('top_ad',$ids); ?></div>
						<?php   if($the_query->post_count > 0) { ?>
                        <div class="articleBlock title center-text">
                           <?php if(!empty($title)) { echo $title; } ?>
                        </div>

                        <div class="articleBlock subtitle center-text">
                        <?php if(!empty($description)) { echo $description; } ?>
                        </div>
						<?php } ?>

                        <div class="related-posts-wrapper padding-xy-10">
                            <div class="related-posts-container">
                                <div class="related-posts  latest-books-listing display-grid" id="lead-data">
                                    <?php
									$ids=array();
                                    if (!empty($the_query)) {
                                        if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                                            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
											$ids[] = $post->ID;                                           
										   ?>

                                            <div class="single-post related-post latest-book-listing">
                                                <div class="image-book p-relative">
                                                     <a href="<?php the_permalink(); ?>"> <img class="image_post"
                                                         src="<?php echo $thumb[0]; ?>"
                                                         srcset="<?php echo $thumb[0]; ?>" alt=""/></a>
                                                    <!--<div class="hover-read-more p-absolute">
                                                        <a href="<?php the_permalink(); ?>"
                                                           class="link-to-book link read-now">
                                                    <span>Read now
                                                        <span class="arrows">
                                                                <span class="lnr lnr-chevron-right"></span>
                                                                <span class="lnr lnr-chevron-right"></span>
                                                        </span>
                                                    </span>
                                                        </a>
                                                    </div> -->
                                                </div>
                                                <a class="link-to-post link" href="<?php the_permalink(); ?>">
                                                    <p class="title-post"><?php the_title(); ?></p>
                                                </a>
                                              <p class="whats-in-post notmobile"><?php  echo get_the_excerpt(); // echo wp_trim_words(get_the_content(), 10); ?></p>
                                            </div>
                                        <?php endwhile;
                                        endif;
                                    } ?>
									<input type="hidden" value="<?php echo $totalPost; ?>" class="totalPosts">
								  
								   <input type="hidden" value="<?php echo serialize($ids); ?>" class="idsval"> 
                         
                                </div>
								<input type="hidden" id="row" value="8">
								<input type="hidden" id="all" value="<?php echo $totalpost; ?>">
				       <?php if (  $the_query->max_num_pages > 1 ) { ?>
                                <div class="display-block button-wrapper load-more-wrapper load-more">
    							<a href="javascript:void(0);" class="link read-now button-design load-more loadmore">Load More</a>
	                          </div>
					   <?php } ?>
                            </div>
                        </div>



                    </div>
<?php echo do_shortcode('[comic_of_the_month_mytho]'); ?>
                    <!-- Other BLocks-->


                    <!--...Other BLocks-->
                </div>
            </div>
        </div>
<div class="adds_footer"><?php echo get_field('bottom_ad',$ids); ?></div>
    </main>
<?php get_footer(); ?>