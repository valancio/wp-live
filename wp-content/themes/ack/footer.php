<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
$buyBooksonline = get_field('buy_books_online','option');
$Newsletter = get_field('newsletter','option');
 dynamic_sidebar( 'social-sharing' );
 //echo '<pre>'; print_r($buyBooksonline); echo '</pre>';
?>
<div class="cart-loading" style="display:none;">
<img src="<?php bloginfo('stylesheet_directory'); ?>/images/ajax-loading.gif"> 
</div>
<?php
if (!is_page('home')) { ?>
    <footer id="site-footer" role="contentinfo" class="header-footer-group">
		 <div class="showCaseWrapper bg-image">
                    <div class="showCaseContainer container-block">
                        <div class="showCaseSection">
                            <div class="content-block">
                                <div class="showCase-title title center-text">
                                    <?php  echo $buyBooksonline['title_buy_book_online'];?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="showCaseContainer image-full-width display-block">
                       <a href="<?php  echo $buyBooksonline['link'];?>"> <img src="<?php  echo $buyBooksonline['image_buy_book_online']['url'];?>"></a>
                    </div>
                </div>

                <div class="newsletterWrapper bg-image">
                    <div class="newsletterContainer container-block">


                        <div class="newsletterSection">
                            <div class="content-block">
                                <div class="newsletter-title title center-text">
                                    <?php echo $Newsletter['title'];?>
                                </div>
                                <div class="newsletter-subtitle subtitle center-text">
                                     <?php echo $Newsletter['description'];?>
                                </div>
                                <div class="form-newsletter">
                                    <?php dynamic_sidebar('newsletter-section') ?> <?php //echo do_shortcode('[mc4wp_form id="101"]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
        <div class="footer-wrapper bg-color-dark top-block">
            <div class="footer-container container-block top">

                <div class="three-block-layout display-grid">

                    <div class="one-third-option">
                        <p class="footer-menu-title">
                            <?php  if ( has_nav_menu( 'ack-media' ) ) { echo wp_get_nav_menu_name("ack-media");  } ?>
                        </p>
                        <?php  if ( has_nav_menu( 'ack-media' ) ) { wp_nav_menu(array('theme_location' => 'ack-media')); } ?>
                    </div>
                    <div class="one-third-option">
                        <p class="footer-menu-title">
                            <?php if ( has_nav_menu( 'customer-service' ) ) { echo wp_get_nav_menu_name("customer-service"); } ?>
                        </p>
                        <?php if ( has_nav_menu( 'customer-service' ) ) { wp_nav_menu(array('theme_location' => 'customer-service'));}  ?>
                    </div>
                    <div class="one-third-option">
                        <?php dynamic_sidebar('sidebar-21'); ?>
                    </div>
                </div>
            </div>
            <div class="footer-container container-block middle">
                <div class="socialMedia-wrapper">

                    <ul class="social-media-links">
                        <?php dynamic_sidebar('sidebar-22'); ?>
                    </ul>
                </div>
            </div>
        </div>


        <div class="footer-wrapper bg-color-dark bottom-block">
            <div class="footer-container container-block bottom">

                <div class="year">© <?php echo date("Y"); ?> </div> <?php  dynamic_sidebar('sidebar-23');   ?>

            </div>
        </div>

    </footer><!-- #site-footer -->

	<script>
	$(document).ready(function() {
   $(".menus-for-burger li").hover(
	function () {
    $('.closed').css('display','none');
	$('.lnr-cross').css('display','none');
	$('.icons-show').css('display','block');
  });
      
 
});
	  $(document).ready(function() {
		    var admin_url = "<?php echo admin_url('admin-ajax.php'); ?>";
       /* $(".continue-reading").click(function(){
		  $(".showmoredefult").hide();
		  $(".showmore").css("display","block");
		  $(".continue-reading").css("display","none");
		  $(".continue-reading-hide").css("display","block");
		  
		});
		
		$(".continue-reading-hide").click(function(){
		  $(".showmoredefult").show();
		  $(".showmore").css("display","none");
		  $(".continue-reading").css("display","block");
		  $(".continue-reading-hide").css("display","none");
		  
		});*/

		
	
	
				
				var page = 2;
               
                $(".loadmore").click(function() {
					var totalPosts = $('.totalPosts').val();
					var idsval = $('.idsval').val();
					var row = Number($('#row').val());
					 var allcount = Number($('#all').val());
					var row = row + 3;	
					var  catid = $(".catname").text();
					var  catslug = $(".catslug").text();
					var  posttypename = $(".posttypename").text();
					$("#row").val(row);
                    $.ajax({
                        type: "POST",
                        url: admin_url,
                        data: {idsval:idsval,page: page, divno: divno,catid: catid,catslug:catslug,posttypename:posttypename, action: 'load_team_by_ajax'},
                        beforeSend: function () {
                            ///$(".cart-loading").show();
                        },
                        success: function (response) { 	
							//response = $.trim(response);
							if(totalPosts==page1){
							$(".cart-loading").hide();
							}
							$('#lead-data').append(response);
								
								page++;						
                            
                        }
                    });
					
                });
				
				
				var page1 = 2;
				$(".load-more-landing").click(function() {
					//var row = Number($('#row').val());
					var totalPosts = $('.totalPosts').val();
					var idsval = $('.idsval').val();
					//alert(idsval);
					 var allcount = Number($('#all').val());
					//var row = row + 3;	
					var  catid = $(".catname").text();
					var  catslug = $(".catslug").text();
					var  posttypename = $(".posttypename").text();
					//$("#row").val(row);
                    $.ajax({
                        type: "POST",
                        url: admin_url,  
                        data: {idsval:idsval,page: page1,catid: catid,catslug:catslug,posttypename:posttypename, action: 'load_team_by_ajax_landing'},
                        beforeSend: function () {
                            //$(".cart-loading").show();
                        },
                        success: function (response) {
						
							if(totalPosts==page1){
							 $(".load-more-landinga").hide();
							}
							$('#lead-data-landing').append(response);
							page1++;
																				
                            
                        }
                    });
					
                });
				
				
				  
				
				
});
		$('.image_of_book img').click(function(){
  $( "._df_button " ).trigger( "click" );
});

	</script>
	<style>
.image_of_book img {
       cursor: pointer;
}
</style>
    <?php
} ?>

<!-- Facebook Pixel Code --> 
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '981668255368237');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=981668255368237&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<?php wp_footer(); ?>
<a id="backToTop"></a>
</body>
</html>
