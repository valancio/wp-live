<?php
/**
 * Handles relevant functions for results pages
 *
 * @package QSM
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class contains functions for loading, saving, and generating results pages.
 *
 * @since 6.2.0
 */
class QSM_Results_Pages {

	/**
	 * Creates the HTML for the results page.
	 *
	 * @since 6.2.0
	 * @param array $response_data The data for the user's submission.
	 * @return string The HTML for the page to be displayed.
	 */
	public static function generate_pages( $response_data ) {
		$pages            = QSM_Results_Pages::load_pages( $response_data['quiz_id'] );
		$result_information = get_field('result_information','option');
		$default1          = $result_information;
		$redirect         = false;
		$default_redirect = false; 
		ob_start();
		
		?>
		<div class="qsm-results-page">
			<?php
			do_action( 'qsm_before_results_page' );
			$page = htmlspecialchars_decode( $default1, ENT_QUOTES );
			echo $page = apply_filters( 'mlw_qmn_template_variable_results_page', $page, $response_data );
			// Cycles through each possible page.
			
			do_action( 'qsm_after_results_page' );
			?>
		</div>
		
		
		<?php
		return array(
			'display'  => do_shortcode( ob_get_clean() ),
			'redirect' => $redirect,
		);

	}

	/**
	 * Loads the results pages for a single quiz.
	 *
	 * @since 6.2.0
	 * @param int $quiz_id The ID for the quiz.
	 * @return bool|array The array of pages or false.
	 */
	public static function load_pages( $quiz_id ) {
		$pages   = array();
		$quiz_id = intval( $quiz_id );

		// If the parameter supplied turns to 0 after intval, returns false.
		if ( 0 === $quiz_id ) {
			return false;
		}

		global $wpdb;
		$results = $wpdb->get_var( $wpdb->prepare( "SELECT message_after FROM {$wpdb->prefix}mlw_quizzes WHERE quiz_id = %d", $quiz_id ) );

		// Checks if the results is an array.
		if ( is_serialized( $results ) && is_array( maybe_unserialize( $results ) ) ) {
			$results = maybe_unserialize( $results );

			// Checks if the results array is not the newer version.
			if ( ! empty( $results ) && ! isset( $results[0]['conditions'] ) ) {
				$pages = QSM_Results_Pages::convert_to_new_system( $quiz_id );
			} else {
				$pages = $results;
			}
		} else {
			$pages = QSM_Results_Pages::convert_to_new_system( $quiz_id );
		}

		return $pages;
	}

	/**
	 * Loads and converts results pages from the old system to new system
	 *
	 * @since 6.2.0
	 * @param int $quiz_id The ID for the quiz.
	 * @return array The combined newer versions of the pages.
	 */
	public static function convert_to_new_system( $quiz_id ) {
		$pages   = array();
		$quiz_id = intval( $quiz_id );

		// If the parameter supplied turns to 0 after intval, returns empty array.
		if ( 0 === $quiz_id ) {
			return $pages;
		}

		/**
		 * Loads the old results pages and converts them.
		 */
		global $wpdb;
		global $mlwQuizMasterNext;
		$data      = $wpdb->get_row( $wpdb->prepare( "SELECT message_after FROM {$wpdb->prefix}mlw_quizzes WHERE quiz_id = %d", $quiz_id ), ARRAY_A );
		$system    = $mlwQuizMasterNext->pluginHelper->get_section_setting( 'quiz_options', 'system', 0 );
		$old_pages = maybe_unserialize( $data['message_after'] );

		// If the value is an array, convert it.
		// If not, use it as the contents of the results page.
		if ( is_array( $old_pages ) ) {

			// Cycle through the older version of results pages.
			foreach ( $old_pages as $page ) {
				$new_page = array(
					'conditions' => array(),
					'page'       => $page[2],
					'redirect'   => false,
				);

				// If the page used the older version of the redirect, add it.
				if ( ! empty( $page['redirect_url'] ) ) {
					$new_page['redirect'] = $page['redirect_url'];
				}

				// Checks to see if the page is not the older version's default page.
				if ( 0 !== intval( $page[0] ) || 0 !== intval( $page[1] ) ) {

					// Checks if the system is points.
					if ( 1 === intval( $system ) ) {
						$new_page['conditions'][] = array(
							'criteria' => 'points',
							'operator' => 'greater-equal',
							'value'    => $page[0],
						);
						$new_page['conditions'][] = array(
							'criteria' => 'points',
							'operator' => 'less-equal',
							'value'    => $page[1],
						);
					} else {
						$new_page['conditions'][] = array(
							'criteria' => 'score',
							'operator' => 'greater-equal',
							'value'    => $page[0],
						);
						$new_page['conditions'][] = array(
							'criteria' => 'score',
							'operator' => 'less-equal',
							'value'    => $page[1],
						);
					}
				}

				$pages[] = $new_page;
			}
		} else {
			$pages[] = array(
				'conditions' => array(),
				'page'       => $old_pages,
				'redirect'   => false,
			);
		}

		// Updates the database with new array to prevent running this step next time.
		$wpdb->update(
			$wpdb->prefix . 'mlw_quizzes',
			array( 'message_after' => serialize( $pages ) ),
			array( 'quiz_id' => $quiz_id ),
			array( '%s' ),
			array( '%d' )
		);

		return $pages;
	}

	/**
	 * Saves the results pages for a quiz.
	 *
	 * @since 6.2.0
	 * @param int   $quiz_id The ID for the quiz.
	 * @param array $pages The results pages to be saved.
	 * @return bool True or false depending on success.
	 */
	public static function save_pages( $quiz_id, $pages ) {
		if ( ! is_array( $pages ) ) {
			return false;
		}

		$quiz_id = intval( $quiz_id );
		if ( 0 === $quiz_id ) {
			return false;
		}

		// Sanitizes data in pages.
		$total = count( $pages );
		for ( $i = 0; $i < $total; $i++ ) {

			// jQuery AJAX will send a string version of false.
			if ( 'false' === $pages[ $i ]['redirect'] ) {
				$pages[ $i ]['redirect'] = false;
			}

			/**
			 * The jQuery AJAX function won't send the conditions key
			 * if it's empty. So, check if it's set. If set, sanitize
			 * data. If not set, set to empty array.
			 */
			if ( isset( $pages[ $i ]['conditions'] ) ) {
				// Sanitizes the conditions.
				$total_conditions = count( $pages[ $i ]['conditions'] );
				for ( $j = 0; $j < $total_conditions; $j++ ) {
					$pages[ $i ]['conditions'][ $j ]['value'] = sanitize_text_field( $pages[ $i ]['conditions'][ $j ]['value'] );
				}
			} else {
				$pages[ $i ]['conditions'] = array();
			}
		}

		global $wpdb;
		$results = $wpdb->update(
			$wpdb->prefix . 'mlw_quizzes',
			array( 'message_after' => serialize( $pages ) ),
			array( 'quiz_id' => $quiz_id ),
			array( '%s' ),
			array( '%d' )
		);
                do_action('qsm_save_result_pages');
		if ( false !== $results ) {
			return true;
		} else {
			return false;
		}
	}
}
?>
