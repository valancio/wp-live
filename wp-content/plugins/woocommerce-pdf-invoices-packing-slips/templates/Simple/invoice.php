	<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<!--<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>
		<td class="shop-info">
			<div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
		</td>
	</tr>
</table>-->
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&display=swap" rel="stylesheet">
    <style>
        a{text-decoration: none;}
        .grand-total{margin-bottom: 20px;}
        .grand-total thead{background: #eaeaea;}
        .grand-total thead th{padding: 4px 17px; font-weight: 400;}
        .grand-total td{padding: 4px 17px; font-size: 12px; white-space: nowrap;}
        .grand-total tfoot td{padding: 2px 10px;}
        .grand-total thead th:last-child, .grand-total tbody td:last-child, .grand-total tfoot td, .grand-total tbody td:nth-child(2), .grand-total thead th:nth-child(2), .grand-total tbody td:nth-child(4){text-align: right;}
    </style>
<h1 class="document-type-label">
<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order );
//echo '<pre>'; print_r($this->order['id']); echo '</pre>';

?>


<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" style="width:500px; margin-left:-20px;">
      <tbody>
        <tr>
            <td style="">
                <table class="main_table" style="width:410px;padding: 20px 0px 0;" bgcolor="#fff" width="410" cellspacing="0" cellpadding="0" align="left">
                    <!--Header section-->
                    <tbody>
                        <tr>
                            <td style="" class="" bgcolor="#ffe900" align="center">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding: 7px 0 7px 14px;">
                                    <tbody>
                                        <tr>
                                            <td><img src="http://34.73.183.204/wp-content/uploads/2020/08/brandlogo.png" alt="" style="max-width: 53px;"></td>
                                            <td style="color:#000;">
                                               <p style="text-transform:uppercase; font-weight: 900; font-size:13px; text-align:end; padding-right: 30px; margin:20px; font-family: 'Roboto', sans-serif;"> India's Favourite Story Teller!</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" style="width:500px; margin:12px 0 7px; padding: 10px 17px; background-color: #737373;" align="center">
                    <tr>
                        <td>
                            <p style="margin: 0 0 5px; color: #fff; font-size: 13px;">Invoice # <?php echo date('Ymd').$this->order->id; ?></p>
                            <p style="margin: 0 0 5px; color: #fff; font-size: 13px;">Order # <?php $this->order_number(); ?> </p>
                            <p style="margin: 0 0 5px; color: #fff; font-size: 13px;">Order Date: <?php $this->order_date(); ?> </p>
                           <!-- <p style="margin: 0; color: #fff; font-size: 13px;">GSTIN :</p>-->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td>
                <table class="card" cellspacing="0" cellpadding="0" style="width:500px; margin-bottom: 10px; " align="center">
                    <tr>
                        <td valign="top" style="border:1px solid #808080;border-right:0px; margin-right: 5px; width: 350px;">
                            <p style="background-color: #edebeb; font-size: 15px; font-weight: 700; padding: 8px 17px; margin:0 ; border-bottom:1px solid #808080;">Sold to:</p>
                            <p style="font-size: 13px; color: #000; padding: 0px 17px; margin: 8px 0; line-height: 20px;"><!-- <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
				<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>	<br>		
			<?php  $this->billing_email(); ?><br>
			<?php  $this->billing_phone(); ?><br>
                            </p>
                        </td>
                        <td valign="top" style="border:1px solid #808080; border-left:0px; margin-right: 5px; width: 350px;">
                            <p style="background-color: #edebeb; font-size: 15px; font-weight: 700; padding: 8px 17px; margin:0 ; border-bottom:1px solid #808080; border-left:1px solid #808080;">Ship to:</p>
                            <p style="font-size: 13px; color: #000; padding: 0px 17px; margin: 8px 0; line-height: 20px;">
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?><br>
				<?php echo "Phone: " . get_post_meta( $this->order->id, '_shipping_phone', true ); ?>
			</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><?php $order = wc_get_order($order_id); ?>
                <table class="card" cellspacing="0" cellpadding="0" style="width:500px; margin-bottom: 20px;" align="center">
                    <tr>
                        <td valign="top" style="border:1px solid #808080; border-right:0px; margin-left: 5px; width: 350px;">
                            <p style="background-color: #edebeb; font-size: 15px; font-weight: 700; padding: 8px 17px; margin:0 ; border-bottom:1px solid #808080;">Payment Method:</p>
                            <p style="font-size: 13px; color: #000; font-weight: 500; padding: 0px 17px; margin: 8px 0;"><?php $this->payment_method(); ?></p>
                        </td>
                        <td valign="top" style="border:1px solid #808080; border-left:0px; margin-right: 5px; width: 350px;">
                            <p style="background-color: #edebeb; font-size: 15px; border-bottom:1px solid #808080; font-weight: 700; padding: 8px 17px; margin:0 ;border-left:1px solid #808080;">Shipping Method:</p>
                            <p style="font-size: 13px; color: #000; padding: 0px 17px; margin: 8px 0 22px;">  <?php foreach( $order->get_items( 'shipping' ) as $item_id => $shipping_item_obj ){ echo $order_item_name  = $shipping_item_obj->get_name(); } ?></p>
                            <p style="font-size: 13px; color: #000; padding: 0px 17px; margin: 0 0 24px;">
							<?php foreach( $order->get_items( 'shipping' ) as $item_id => $shipping_item_obj ){
  
								//echo $shipping_method_title       = $shipping_item_obj->get_method_title(); 
								echo '(Total Shipping Charges Rs. '.$shipping_method_total = $shipping_item_obj->get_total().'): ';
							} ?>
							</p>
                        </td>
                    </tr>
                </table>
            </td>
         </tr>
        <tr>
            <td>
                <table style="width:760px;" cellspacing="0" cellpadding="0" align="center" class="grand-total">
                    <thead style="font-size: 12px; font-weight: 900; text-align: left; ">
                        <th style="border:1px solid #808080; border-right: 0px;">Products</th>
                       <!-- <th style="border:1px solid #808080; border-right: 0px; border-left:0;">SKU</th>-->
                        <th style="border:1px solid #808080; border-right: 0px; border-left:0; text-align: left;">Price</th>
                        <th style="border:1px solid #808080; border-right: 0px; border-left:0;">Qty</th>
                        <th style="border:1px solid #808080; border-left: 0px;">Subtotal</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
						
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
			<td class="product">
				<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<!--<td><?php //echo $item['sku']; ?></td>-->
			<td class="price" style="text-align: left;"><?php echo $item['order_price']; ?></td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td><?php echo $item['order_price']; ?></td>
			
		</tr>
			<?php endforeach; endif; ?>
                        
                    </tbody>
                  </table> 
				   </td><td>
				<table class="totals" style="margin-left:-170px; width:50px; margin-top:50px;">
				<tbody>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
						<tr class="<?php echo $key; ?>">
					
							  <td colspan="4" style="font-weight: 700; border-top:0px; border-bottom: 1px solid #000;"><p style="margin: 30px 0 0;"><?php echo $total['label']; ?></td>
							 <td style="font-weight: 700; border-top:0px; border-bottom: 1px solid #000;"><p style="margin: 30px 0 0; "><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
						</tbody>
				</table></td>

      </tbody>
      </table>
	  
<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
